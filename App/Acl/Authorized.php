<?php

namespace App\Acl;

use App\Acl\Resources;
use App\Acl\Roles;
use Laminas\Permissions\Acl\Acl;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Authorized
{
    private $acl;
    private $resources;
    private $roles;

    public function __construct()
    {
        $this->acl = new Acl();
        $this->resources = new Resources($this->acl);
        $this->roles = new Roles($this->acl);
    }

    public function __invoke(Request $request, Response $response, $next)
    {
        $route = $request->getAttribute('route');
        $callable = explode(':', $route->getCallable());
        $decoded = $request->getAttribute('decoded_token_data');

        if (!$this->acl->isAllowed($decoded['nivelAcesso'], $callable[0], $callable[1])) {
            $message = [
                'status' => 403,
                'authorized_error' => true,
                'message' => 'Unauthorized user',
            ];
            return $response->withStatus(403)->withJson($message);
        }

        $response = $next($request, $response);

        return $response;
    }
}
