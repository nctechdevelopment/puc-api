<?php

namespace App\Acl;

use App\Controllers\AccessController;
use App\Controllers\DoctorController;
use App\Controllers\LaboratoryController;
use App\Controllers\MedicalController;
use App\Controllers\MedicamentController;
use App\Controllers\ReceptionistController;
use App\Controllers\ScheduleController;
use App\Controllers\PatientController;
use Laminas\Permissions\Acl\Acl;
use Laminas\Permissions\Acl\Resource\GenericResource;

class Resources
{
    public function __construct(Acl $acl)
    {
        $acl->addResource(new GenericResource(AccessController::class));
        $acl->addResource(new GenericResource(DoctorController::class));
        $acl->addResource(new GenericResource(LaboratoryController::class));
        $acl->addResource(new GenericResource(MedicalController::class));
        $acl->addResource(new GenericResource(MedicamentController::class));
        $acl->addResource(new GenericResource(ReceptionistController::class));
        $acl->addResource(new GenericResource(ScheduleController::class));
        $acl->addResource(new GenericResource(PatientController::class));
    }
}
