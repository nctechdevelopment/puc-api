<?php

namespace App\Acl;

use App\Controllers\AccessController;
use App\Controllers\DoctorController;
use App\Controllers\ExamController;
use App\Controllers\LaboratoryController;
use App\Controllers\MedicalController;
use App\Controllers\MedicamentController;
use App\Controllers\ReceptionistController;
use App\Controllers\ScheduleController;
use App\Controllers\PatientController;
use Laminas\Permissions\Acl\Acl;
use Laminas\Permissions\Acl\Role\GenericRole;

class Roles
{
    public function __construct(Acl $acl)
    {
        // Roles
        $acl->addRole(new GenericRole('1')); // 'Administradores'
        $acl->addRole(new GenericRole('2')); // 'Laboratórios'
        $acl->addRole(new GenericRole('3')); // 'Médicos'
        $acl->addRole(new GenericRole('4')); // 'Recepcionistas'
        $acl->addRole(new GenericRole('5')); // 'Pacientes'

        // Privileges to access doctors
        $acl->allow('1', AccessController::class, ['refreshToken', 'changePasswordSafely', 'changePassword']);
        $acl->allow('2', AccessController::class, ['refreshToken', 'changePasswordSafely']);
        $acl->allow('3', AccessController::class, ['refreshToken', 'changePasswordSafely']);
        $acl->allow('4', AccessController::class, ['refreshToken', 'changePasswordSafely']);
        // Privileges to access doctors
        $acl->allow('1', DoctorController::class, ['searchAll', 'register', 'update', 'findById', 'updateStatus']);
        $acl->allow('3', DoctorController::class, ['update', 'findById']);
        // Privileges to access laboratories
        $acl->allow('1', LaboratoryController::class, ['searchAll', 'register', 'update', 'findById', 'updateStatus']);
        $acl->allow('2', LaboratoryController::class, ['update', 'findById']);
        // Privileges to access medicals
        $acl->allow('2', MedicalController::class, ['uploadExam', 'downloadExam']);
        $acl->allow('5', MedicalController::class, ['downloadExam']);
        $acl->allow('3', MedicalController::class, ['registerMedical', 'findRecordByScheduleId', 'findExamByRecordId',
            'findPrescriptionByRecordId', 'findCertificateByRecordId', 'searchExamTypes', 'findRecordHistoricByPatientId',
            'findExamHistoricByPatientId', 'findPrescriptionHistoricByPatientId', 'findCertificateHistoricByPatientId']);
        // Privileges to access medicaments
        $acl->allow('1', MedicamentController::class, ['searchAll', 'register', 'update', 'findById', 'search', 'updateStatus']);
        $acl->allow('3', MedicamentController::class, ['search']);
        // Privileges to access receptionists
        $acl->allow('1', ReceptionistController::class, ['searchAll', 'register', 'update', 'findById', 'updateStatus']);
        $acl->allow('4', ReceptionistController::class, ['update', 'findById']);
        // Privileges to access schedules
        $acl->allow('1', ScheduleController::class, ['register', 'findAllByDoctor', 'update', 'updateEvent', 'delete']);
        $acl->allow('3', ScheduleController::class, ['register', 'findAllByDoctor', 'findById', 'update', 'updateEvent', 'delete']);
        $acl->allow('4', ScheduleController::class, ['register', 'findAllDoctors', 'findAllByDoctor', 'update', 'updateEvent', 'delete']);
        // Privileges to access patients
        $acl->allow('1', PatientController::class, ['searchAll', 'register', 'update', 'findById', 'search', 'updateStatus']);
        $acl->allow('3', PatientController::class, ['searchAll', 'register', 'update', 'findById', 'search', 'updateStatus']);
        $acl->allow('4', PatientController::class, ['searchAll', 'register', 'update', 'findById', 'search', 'updateStatus']);
    }
}
