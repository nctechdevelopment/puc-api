<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Models\Mappers\AccessMapper;
use Firebase\JWT\JWT;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class AccessController extends Controller
{
    private $secretKey;
    private $accessMapper;

    public function __construct()
    {
        parent::__construct();
        $this->accessMapper = new AccessMapper();
        $this->secretKey = $this->config['secret_key'];
    }

    public function login(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        $result = $this->accessMapper->login($data);

        if ($result['status'] !== 200) {
            return $response->withStatus($result['status'])->withJson($result);
        }

        $now = time();
        $future = time() + (8 * 60 * 60); // 8 hours

        $user = $result['user'];
        $login = $result['login'];
        $tokenPayload = [
            'id' => $user['id'],
            'loginId' => $login['id'],
            'nome' => $user['nome'],
            'imagem' => $user['imagem'],
            'email' => $login['email'],
            'nivelAcesso' => $login['nivel_acesso'],
            'iat' => $now,
            'exp' => $future,
        ];

        $token = JWT::encode($tokenPayload, $this->secretKey);

        return $response->withStatus($result['status'])->withJson(['status' => $result['status'], 'message' => $result['message'], 'token' => $token]);
    }

    public function refreshToken(Request $request, Response $response, array $args)
    {
        $decodedTokenData = $request->getAttribute('decoded_token_data');
        $result = $this->accessMapper->findById($decodedTokenData['loginId']);

        if ($result['status'] !== 200) {
            return $response->withStatus($result['status'])->withJson($result);
        }

        $user = $result['user'];
        $login = $result['login'];
        $tokenPayload = [
            'id' => $user['id'],
            'loginId' => $login['id'],
            'nome' => $user['nome'],
            'imagem' => $user['imagem'],
            'email' => $login['email'],
            'nivelAcesso' => $login['nivel_acesso'],
            'iat' => $decodedTokenData['iat'],
            'exp' => $decodedTokenData['exp'],
        ];

        $token = JWT::encode($tokenPayload, $this->secretKey);

        return $response->withStatus($result['status'])->withJson(['status' => 200, 'token' => $token]);
    }

    public function changePassword(Request $request, Response $response, array $args)
    {
        $data = $request->getParsedBody();
        $result = $this->accessMapper->changePassword($data, $args['id']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function changePasswordSafely(Request $request, Response $response, array $args)
    {
        $data = $request->getParsedBody();
        $result = $this->accessMapper->changePasswordSafely($data, $args['id']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function loginExam(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        $result = $this->accessMapper->loginExam($data);

        if ($result['status'] !== 200) {
            return $response->withStatus($result['status'])->withJson($result);
        }

        $now = time();
        $future = time() + (1 * 60 * 60); // 1 hour

        $user = $result['user'];
        $tokenPayload = [
            'loginId' => $user['id'],
            'nome' => $user['nome'],
            'imagem' => $user['imagem'],
            'email' => $user['email'],
            'id' => $user['exames_id'],
            'nivelAcesso' => '5',
            'iat' => $now,
            'exp' => $future,
        ];

        $token = JWT::encode($tokenPayload, $this->secretKey);

        return $response->withStatus($result['status'])->withJson(['status' => $result['status'], 'message' => $result['message'], 'token' => $token]);
    }

}
