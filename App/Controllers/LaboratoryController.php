<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Helpers\ProcessImage;
use App\Models\Mappers\LaboratoryMapper;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class LaboratoryController extends Controller
{
    private $serverName;
    private $processImage;
    private $laboratoryMapper;

    public function __construct()
    {
        parent::__construct();
        $this->processImage = new ProcessImage();
        $this->serverName = $this->config['server_name'];
        $this->laboratoryMapper = new LaboratoryMapper();
    }

    public function searchAll(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        $result = $this->laboratoryMapper->searchAll($data);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function register(Request $request, Response $response)
    {
        $image = $request->getUploadedFiles();
        $data = $request->getParsedBody();

        $path = dirname(__FILE__) . '/../../public/profile-image/laboratories/';
        $pathUrl = $this->serverName . '/public/profile-image/laboratories/';

        if (!isset($image['image'])) {
            $result = $this->laboratoryMapper->register($data, null);
        } else {
            $resultFilename = $this->processImage->process($image['image'], $path);
            if (!$resultFilename['status']) {
                unlink($path . $resultFilename['filename']);
                return $response->withStatus(500)->withJson(['status' => 500, 'message' => 'Desculpe, ocorreu um erro interno.']);
            }

            $result = $this->laboratoryMapper->register($data, $pathUrl . $resultFilename['filename']);
            if ($result['status'] != 201) {unlink($path . $resultFilename['filename']);}
        }

        return $response->withStatus($result['status'])->withJson($result);
    }

    public function update(Request $request, Response $response)
    {
        $image = $request->getUploadedFiles();
        $data = $request->getParsedBody();

        $path = dirname(__FILE__) . '/../../public/profile-image/laboratories/';
        $pathUrl = $this->serverName . '/public/profile-image/laboratories/';

        if (!isset($image['image'])) {
            $result = $this->laboratoryMapper->update($data, isset($data['imagemAntiga']) ? $data['imagemAntiga'] : null );
        } else {
            $resultFilename = $this->processImage->process($image['image'], $path);
            if (!$resultFilename['status']) {
                unlink($path . $resultFilename['filename']);
                return $response->withStatus(500)->withJson(['status' => 500, 'message' => 'Desculpe, ocorreu um erro interno.']);
            }

            $result = $this->laboratoryMapper->update($data, $pathUrl . $resultFilename['filename']);
            if ($result['status'] != 200) {
                unlink($path . $resultFilename['filename']);
            } else if (isset($data['imagemAntiga'])) {
                $filename = substr(strrchr($data['imagemAntiga'], "/"), 1);
                unlink($path . $filename);
            }
        }

        return $response->withStatus($result['status'])->withJson($result);
    }

    public function findById(Request $request, Response $response, array $args)
    {
        $result = $this->laboratoryMapper->findById($args['id']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function updateStatus(Request $request, Response $response, array $args)
    {
        $result = $this->laboratoryMapper->updateStatus($args['id'], $args['status']);
        return $response->withStatus($result['status'])->withJson($result);
    }

}
