<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Helpers\ProcessImage;
use App\Models\Mappers\MedicalMapper;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class MedicalController extends Controller
{
    private $serverName;
    private $processFile;
    private $medicalMapper;

    public function __construct()
    {
        parent::__construct();
        $this->serverName = $this->config['server_name'];
        $this->processFile = new ProcessImage();
        $this->medicalMapper = new MedicalMapper();
    }

    public function registerMedical(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        $result = $this->medicalMapper->registerMedical($data);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function uploadExam(Request $request, Response $response)
    {
        $file = $request->getUploadedFiles();
        $data = $request->getParsedBody();

        $path = dirname(__FILE__) . '/../../public/exams/';
        $pathUrl = $this->serverName . '/public/exams/';

        if (!isset($file['file'])) {
            return $response->withStatus(500)->withJson(['status' => 500, 'message' => 'Nenhum resultado anexado.']);
        } else {
            $resultFilename = $this->processFile->process($file['file'], $path);
            if (!$resultFilename['status']) {
                unlink($path . $resultFilename['filename']);
                return $response->withStatus(500)->withJson(['status' => 500, 'message' => 'Desculpe, ocorreu um erro interno.']);
            }

            $result = $this->medicalMapper->uploadExam($data['id'], $pathUrl . $resultFilename['filename']);
            if ($result['status'] != 200) {unlink($path . $resultFilename['filename']);}
        }

        return $response->withStatus($result['status'])->withJson($result);
    }

    public function downloadExam(Request $request, Response $response, array $args)
    {
        $result = $this->medicalMapper->downloadExam($args['id']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function findRecordByScheduleId(Request $request, Response $response, array $args)
    {
        $result = $this->medicalMapper->findRecordByScheduleId($args['id']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function findRecordHistoricByPatientId(Request $request, Response $response, array $args)
    {
        $result = $this->medicalMapper->findRecordHistoricByPatientId($args['id'], $args['page']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function findExamByRecordId(Request $request, Response $response, array $args)
    {
        $result = $this->medicalMapper->findExamByRecordId($args['id']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function findExamHistoricByPatientId(Request $request, Response $response, array $args)
    {
        $result = $this->medicalMapper->findExamHistoricByPatientId($args['id'], $args['page']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function findPrescriptionByRecordId(Request $request, Response $response, array $args)
    {
        $result = $this->medicalMapper->findPrescriptionByRecordId($args['id']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function findPrescriptionHistoricByPatientId(Request $request, Response $response, array $args)
    {
        $result = $this->medicalMapper->findPrescriptionHistoricByPatientId($args['id'], $args['page']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function findCertificateByRecordId(Request $request, Response $response, array $args)
    {
        $result = $this->medicalMapper->findCertificateByRecordId($args['id']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function findCertificateHistoricByPatientId(Request $request, Response $response, array $args)
    {
        $result = $this->medicalMapper->findCertificateHistoricByPatientId($args['id'], $args['page']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function searchExamTypes(Request $request, Response $response, array $args)
    {
        $result = $this->medicalMapper->searchExamTypes($args['search']);
        return $response->withStatus($result['status'])->withJson($result);
    }

}
