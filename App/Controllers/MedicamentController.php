<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Models\Mappers\MedicamentMapper;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class MedicamentController extends Controller
{
    private $medicamentMapper;

    public function __construct()
    {
        $this->medicamentMapper = new MedicamentMapper();
    }

    public function searchAll(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        $result = $this->medicamentMapper->searchAll($data);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function register(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        $result = $this->medicamentMapper->register($data);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function update(Request $request, Response $response, array $args)
    {
        $data = $request->getParsedBody();
        $result = $this->medicamentMapper->update($args['id'], $data);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function findById(Request $request, Response $response, array $args)
    {
        $result = $this->medicamentMapper->findById($args['id']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function search(Request $request, Response $response, array $args)
    {
        $result = $this->medicamentMapper->search($args['search']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function updateStatus(Request $request, Response $response, array $args)
    {
        $result = $this->medicamentMapper->updateStatus($args['id'], $args['status']);
        return $response->withStatus($result['status'])->withJson($result);
    }

}
