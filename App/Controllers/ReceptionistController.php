<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Helpers\ProcessImage;
use App\Models\Mappers\ReceptionistMapper;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ReceptionistController extends Controller
{
    private $serverName;
    private $processImage;
    private $receptionistMapper;

    public function __construct()
    {
        parent::__construct();
        $this->serverName = $this->config['server_name'];
        $this->processImage = new ProcessImage();
        $this->receptionistMapper = new ReceptionistMapper();
    }

    public function searchAll(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        $result = $this->receptionistMapper->searchAll($data);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function register(Request $request, Response $response)
    {
        $image = $request->getUploadedFiles();
        $data = $request->getParsedBody();

        $path = dirname(__FILE__) . '/../../public/profile-image/receptionists/';
        $pathUrl = $this->serverName . '/public/profile-image/receptionists/';

        if (!isset($image['image'])) {
            $result = $this->receptionistMapper->register($data, null);
        } else {
            $resultFilename = $this->processImage->process($image['image'], $path);
            if (!$resultFilename['status']) {
                unlink($path . $resultFilename['filename']);
                return $response->withStatus(500)->withJson(['status' => 500, 'message' => 'Desculpe, ocorreu um erro interno.']);
            }

            $result = $this->receptionistMapper->register($data, $pathUrl . $resultFilename['filename']);
            if ($result['status'] != 201) {unlink($path . $resultFilename['filename']);}
        }

        return $response->withStatus($result['status'])->withJson($result);
    }

    public function update(Request $request, Response $response)
    {
        $image = $request->getUploadedFiles();
        $data = $request->getParsedBody();

        $path = dirname(__FILE__) . '/../../public/profile-image/receptionists/';
        $pathUrl = $this->serverName . '/public/profile-image/receptionists/';

        if (!isset($image['image'])) {
            $result = $this->receptionistMapper->update($data, isset($data['imagemAntiga']) ? $data['imagemAntiga'] : null);
        } else {
            $resultFilename = $this->processImage->process($image['image'], $path);
            if (!$resultFilename['status']) {
                unlink($path . $resultFilename['filename']);
                return $response->withStatus(500)->withJson(['status' => 500, 'message' => 'Desculpe, ocorreu um erro interno.']);
            }

            $result = $this->receptionistMapper->update($data, $pathUrl . $resultFilename['filename']);
            if ($result['status'] != 200) {
                unlink($path . $resultFilename['filename']);
            } else if (isset($data['imagemAntiga'])) {
                $filename = substr(strrchr($data['imagemAntiga'], "/"), 1);
                unlink($path . $filename);
            }
        }

        return $response->withStatus($result['status'])->withJson($result);
    }

    public function findById(Request $request, Response $response, array $args)
    {
        $result = $this->receptionistMapper->findById($args['id']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function updateStatus(Request $request, Response $response, array $args)
    {
        $result = $this->receptionistMapper->updateStatus($args['id'], $args['status']);
        return $response->withStatus($result['status'])->withJson($result);
    }

}
