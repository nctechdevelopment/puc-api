<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Models\Mappers\ScheduleMapper;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ScheduleController extends Controller
{
    private $scheduleMapper;

    public function __construct()
    {
        $this->scheduleMapper = new ScheduleMapper();
    }

    public function register(Request $request, Response $response, array $args)
    {
        $data = $request->getParsedBody();
        $result = $this->scheduleMapper->register($data);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function findAllDoctors(Request $request, Response $response, array $args)
    {
        $result = $this->scheduleMapper->findAllDoctors();
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function findAllByDoctor(Request $request, Response $response, array $args)
    {
        $result = $this->scheduleMapper->findAllByDoctor($args['id'], $args['start'], $args['end']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function findById(Request $request, Response $response, array $args)
    {
        $result = $this->scheduleMapper->findById($args['id']);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function update(Request $request, Response $response, array $args)
    {
        $data = $request->getParsedBody();
        $result = $this->scheduleMapper->update($args['id'], $data);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function updateEvent(Request $request, Response $response, array $args)
    {
        $data = $request->getParsedBody();
        $result = $this->scheduleMapper->updateEvent($args['id'], $data);
        return $response->withStatus($result['status'])->withJson($result);
    }

    public function delete(Request $request, Response $response, array $args)
    {
        $result = $this->scheduleMapper->delete($args['id']);
        return $response->withStatus($result['status'])->withJson($result);
    }

}
