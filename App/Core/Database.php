<?php

namespace App\Core;

use PDO as PDO;

class Database
{

    private $pdo;
    private $config;

    /**
     * Constructor of the class
     */
    public function __construct()
    {
        $this->config = include(dirname(__FILE__) . "/../../config.php");

        $this->pdo = new PDO(
            "mysql:host=" . $this->config["db"]["host"] . ";
            dbname=" . $this->config["db"]["database"] . ";
            charset=utf8",
            $this->config["db"]["user"],
            $this->config["db"]["password"]
        );

        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'utf8'");
    }

    /**
     * Method responsible to return an PDO instance
     *
     * @return PDO pdo instance to interact with database
     */
    public function getInstance()
    {
        return $this->pdo;
    }

}
