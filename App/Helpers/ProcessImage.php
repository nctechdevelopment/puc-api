<?php

namespace App\Helpers;

class ProcessImage
{

    public function process($file, $path)
    {
        $extension = pathinfo($file->getClientFilename(), PATHINFO_EXTENSION);

        do {
            $basename = bin2hex(random_bytes(8));
            $filename = sprintf('%s.%0.8s', $basename, $extension);
        } while (file_exists($path . $filename));

        if (!move_uploaded_file($file->file, $path . $filename)) {
            return ['status' => false, 'filename' => $filename];
        }

        return ['status' => true, 'filename' => $filename];
    }

}
