<?php

namespace App\Helpers;

use App\Core\Database;
use PDO as PDO;

class Utils
{

    /**
     * PDO Object to interact with database
     *
     * @var PDO
     */
    private $db;

    /**
     * Constructor of the class
     */
    public function __construct()
    {
        // Opening database connection
        $this->db = new Database();
        $this->db = $this->db->getInstance();
    }

    public function checkDuplicates($data, $tables, $fields, $words, $id = null)
    {
        $concat = '';
        $checkMessage = '';

        for ($i = 0; $i < count($fields); $i++) {
            // IF the variable has been declared
            if (isset($data[$this->camelToSnake($fields[$i])])) {
                $concat = '';

                $sql = 'SELECT id FROM ' . (is_array($tables) ? $tables[$i] : $tables) . ' WHERE ' . $fields[$i] . ' = ?';
                if ($id) {$concat = ' AND id != ? ';}
                $sql = $sql . $concat . 'LIMIT 1';

                $checkQuery = $this->db->prepare($sql);
                $checkQuery->bindValue(1, $data[$this->camelToSnake($fields[$i])]);
                if ($id) {$checkQuery->bindValue(2, $id);}

                if (!$checkQuery->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

                $checking = $checkQuery->fetchAll(PDO::FETCH_ASSOC);
                if (!empty($checking)) {
                    $checkMessage = $checkMessage . $words[$i] . ', ';
                }
            }
        }

        return $checkMessage;
    }

    public function camelToSnake($field)
    {
        return str_replace('_', '', lcfirst(ucwords($field, '_')));
    }

    public function isDate($string)
    {
        $matches = array();
        $patternDate = '/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4})$/';
        if (!preg_match($patternDate, $string, $matches)) {return false;}
        return true;
    }

    public function isDateTime($string)
    {
        $matches = array();
        $patternDate = '/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4}) ([0-9]{2})\:([0-9]{2})$/';
        if (!preg_match($patternDate, $string, $matches)) {return false;}
        return true;
    }

    public function formatDateBrToEn($timestamp)
    {
        $formatTimestamp = str_replace('/', '-', $timestamp);
        return date('Y-m-d', strtotime($formatTimestamp));
    }

    public function formatDateTimeBrToEn($timestamp)
    {
        $formatTimestamp = str_replace('/', '-', $timestamp);
        return date('Y-m-d H:i', strtotime($formatTimestamp));
    }

}
