<?php
namespace App\Models\Mappers;

use App\Core\Database;
use PDO as PDO;

class AccessMapper
{
    /**
     * PDO Object to interact with database
     *
     * @var PDO
     */
    private $db;

    /**
     * Constructor of the class
     */
    public function __construct()
    {
        // Opening database connection
        $this->db = new Database();
        $this->db = $this->db->getInstance();
    }

    public function login($data)
    {
        // Login
        $sqlLogin = 'SELECT * FROM login WHERE email = ? LIMIT 1';
        $queryLogin = $this->db->prepare($sqlLogin);
        $queryLogin->bindValue(1, $data['email']);

        if (!$queryLogin->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $resultLogin = $queryLogin->fetch(\PDO::FETCH_ASSOC);

        if ($queryLogin->rowCount() === 0 || !password_verify($data['senha'], $resultLogin['senha'])) {
            return [
                'status' => 401,
                'message' => 'E-mail ou senha inválidos.',
            ];
        }

        unset($resultLogin['senha']);

        // Table
        switch ($resultLogin['nivel_acesso']) {
            case '1':
                $tableName = 'administradores';
                break;
            case '2':
                $tableName = 'laboratorios';
                break;
            case '3':
                $tableName = 'medicos';
                break;
            case '4':
                $tableName = 'recepcionistas';
                break;
        }

        // User
        $sqlUser = 'SELECT id, nome, imagem, status FROM ' . $tableName . ' WHERE login_id = ? LIMIT 1';
        $queryUser = $this->db->prepare($sqlUser);
        $queryUser->bindValue(1, $resultLogin['id']);

        if (!$queryUser->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $resultUser = $queryUser->fetch(\PDO::FETCH_ASSOC);

        if ($resultUser['status'] === '0') {
            return [
                'status' => 401,
                'message' => 'Você não tem permissão para realizar o login, consulte o administrador.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Login realizado com sucesso.',
            'login' => $resultLogin,
            'user' => $resultUser,
        ];
    }

    public function findById($id)
    {
        // Login
        $sqlLogin = 'SELECT * FROM login WHERE id = ? LIMIT 1';
        $queryLogin = $this->db->prepare($sqlLogin);
        $queryLogin->bindValue(1, $id);

        if (!$queryLogin->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $resultLogin = $queryLogin->fetch(\PDO::FETCH_ASSOC);

        if ($queryLogin->rowCount() === 0) {
            return [
                'status' => 404,
                'message' => 'Usuário não encontrado.',
            ];
        }

        unset($resultLogin['senha']);

        // Table
        switch ($resultLogin['nivel_acesso']) {
            case '1':
                $tableName = 'administradores';
                break;
            case '2':
                $tableName = 'laboratorios';
                break;
            case '3':
                $tableName = 'medicos';
                break;
            case '4':
                $tableName = 'recepcionistas';
                break;
        }

        // User
        $sqlUser = 'SELECT id, nome, imagem, status FROM ' . $tableName . ' WHERE login_id = ? LIMIT 1';
        $queryUser = $this->db->prepare($sqlUser);
        $queryUser->bindValue(1, $resultLogin['id']);

        if (!$queryUser->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $resultUser = $queryUser->fetch(\PDO::FETCH_ASSOC);

        if ($resultUser['status'] === '0') {
            return [
                'status' => 401,
                'message' => 'Você não tem permissão para realizar o login, consulte o administrador.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Login encontrado com sucesso.',
            'login' => $resultLogin,
            'user' => $resultUser,
        ];
    }

    public function register($user)
    {
        $hash = password_hash($user['login']['senha'], PASSWORD_BCRYPT, array('cost' => 10));

        $sql = 'INSERT INTO `login`(`email`, `senha`, `nivel_acesso`) VALUES (?, ?, ?)';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $user['login']['email']);
        $query->bindValue(2, $hash);
        $query->bindValue(3, $user['login']['nivelAcesso']);

        if (!$query->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

        return [
            'status' => 200,
            'message' => 'Login cadastrado com sucesso.',
            'lastInsertId' => $this->db->lastInsertId(),
        ];
    }

    public function changePassword($data, $id)
    {
        // Get password
        $sqlPassword = 'SELECT senha FROM login WHERE id = ? LIMIT 1';
        $queryPassword = $this->db->prepare($sqlPassword);
        $queryPassword->bindValue(1, $id);

        if (!$queryPassword->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $resultPassword = $queryPassword->fetch(\PDO::FETCH_ASSOC);

        if ($queryPassword->rowCount() === 0) {
            return [
                'status' => 404,
                'message' => 'Usuário não encontrado.',
            ];
        }

        // Validation
        $message = '';
        if ($data['novaSenha'] !== $data['confirmarSenha']) {
            $message = $message . 'Senha e confirmação de senha não conferem.<br />';
        }

        if (password_verify($data['novaSenha'], $resultPassword['senha'])) {
            $message = $message . 'Você deve escolher uma senha diferente da atual.';
        }

        if ($message) {
            return [
                'status' => 403,
                'message' => $message,
            ];
        }

        // Update passowrd
        $hash = password_hash($data['novaSenha'], PASSWORD_BCRYPT, array('cost' => 10));

        $sql = 'UPDATE login SET senha = ? WHERE id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $hash);
        $query->bindValue(2, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Senha atualizada com sucesso.',
        ];
    }

    public function changePasswordSafely($data, $id)
    {
        // Get password
        $sqlPassword = 'SELECT senha FROM login WHERE id = ? LIMIT 1';
        $queryPassword = $this->db->prepare($sqlPassword);
        $queryPassword->bindValue(1, $id);

        if (!$queryPassword->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $resultPassword = $queryPassword->fetch(\PDO::FETCH_ASSOC);

        if ($queryPassword->rowCount() === 0) {
            return [
                'status' => 404,
                'message' => 'Usuário não encontrado.',
            ];
        }

        // Validation
        $message = '';
        if ($data['novaSenha'] !== $data['confirmarSenha']) {
            $message = $message . 'Senha e confirmação de senha não conferem.<br />';
        }

        if (!password_verify($data['senha'], $resultPassword['senha'])) {
            $message = $message . 'A senha atual está incorreta.<br />';
        }

        if (password_verify($data['novaSenha'], $resultPassword['senha'])) {
            $message = $message . 'Você deve escolher uma senha diferente da atual.';
        }

        if ($message) {
            return [
                'status' => 403,
                'message' => $message,
            ];
        }

        // Update password
        $hash = password_hash($data['novaSenha'], PASSWORD_BCRYPT, array('cost' => 10));

        $sql = 'UPDATE login SET senha = ? WHERE id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $hash);
        $query->bindValue(2, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Senha atualizada com sucesso.',
        ];
    }

    public function loginExam($data)
    {
        // Login
        $sql = 'SELECT e.id AS `exames_id`, p.id, p.nome, p.email, p.imagem
                FROM exames AS `e`
                JOIN prontuarios AS `pr` ON pr.id = e.prontuarios_id
                JOIN pacientes AS `p` ON p.id = pr.pacientes_id
                WHERE login = ? AND senha = ? LIMIT 1';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $data['login']);
        $query->bindValue(2, $data['senha']);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetch(\PDO::FETCH_ASSOC);

        if ($query->rowCount() === 0) {
            return [
                'status' => 401,
                'message' => 'Login ou senha inválidos.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Login realizado com sucesso.',
            'user' => $result,
        ];
    }

}
