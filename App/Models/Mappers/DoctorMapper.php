<?php
namespace App\Models\Mappers;

use App\Core\Database;
use App\Helpers\Utils;
use App\Models\Mappers\AccessMapper;
use Exception;
use PDO as PDO;

class DoctorMapper
{
    /**
     * PDO Object to interact with database
     *
     * @var PDO
     */
    private $db;

    /**
     * Other variables
     */
    private $utils;
    private $accessMapper;

    /**
     * Constructor of the class
     */
    public function __construct()
    {
        // Opening database connection
        $this->db = new Database();
        $this->db = $this->db->getInstance();
        // Other variables
        $this->utils = new Utils();
        $this->accessMapper = new AccessMapper();
    }

    public function searchAll($data)
    {
        $concat = '';
        $perPage = $data['length'];
        $offset = ($perPage * $data['page']) - $perPage;
        $searchString = '%' . $data['search'] . '%';
        $searchData = '';

        if ($this->utils->isDate($data['search'])) {
            $splitTimestamp = explode(' ', $data['search']);
            $format = date('Y-m-d', strtotime(str_replace('/', '-', $splitTimestamp[0])));
            $searchData = '%' . $format . (isset($splitTimestamp[1]) ? ' ' . $splitTimestamp[1] : '') . '%';
        }

        // Doctors
        $sql = 'SELECT m.*, e.especialidades FROM medicos AS `m` ';
        $concat = $concat . 'JOIN especialidades_medicos AS `e` ON e.medicos_id = m.id ';
        $concat = $concat . 'WHERE m.nome LIKE ? OR m.data_nascimento LIKE ? OR
            m.cpf LIKE ? OR m.rg LIKE ? OR m.crm LIKE ? OR m.cep LIKE ? OR
            m.estado LIKE ? OR m.cidade LIKE ? OR m.bairro LIKE ? OR
            m.logradouro LIKE ? OR m.complemento LIKE ? OR m.numero LIKE ? OR
            m.celular LIKE ? OR m.telefone LIKE ? OR m.email LIKE ? OR e.especialidades LIKE ? ';
        $concat = $concat . 'GROUP BY m.id ORDER BY m.status DESC, m.id ASC ';
        $sql = $sql . $concat . 'LIMIT ? OFFSET ?';

        $query = $this->db->prepare($sql);
        $query->bindValue(1, $searchString);
        $query->bindValue(2, $searchData);
        $query->bindValue(3, $searchString);
        $query->bindValue(4, $searchString);
        $query->bindValue(5, $searchString);
        $query->bindValue(6, $searchString);
        $query->bindValue(7, $searchString);
        $query->bindValue(8, $searchString);
        $query->bindValue(9, $searchString);
        $query->bindValue(10, $searchString);
        $query->bindValue(11, $searchString);
        $query->bindValue(12, $searchString);
        $query->bindValue(13, $searchString);
        $query->bindValue(14, $searchString);
        $query->bindValue(15, $searchString);
        $query->bindValue(16, $searchString);
        $query->bindValue(17, (int) $perPage, PDO::PARAM_INT);
        $query->bindValue(18, (int) $offset, PDO::PARAM_INT);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        // Count
        $sqlCount = 'SELECT m.*, e.especialidades FROM medicos AS `m` ';
        $sqlCount = $sqlCount . $concat;

        $queryCount = $this->db->prepare($sqlCount);
        $queryCount->bindValue(1, $searchString);
        $queryCount->bindValue(2, $searchData);
        $queryCount->bindValue(3, $searchString);
        $queryCount->bindValue(4, $searchString);
        $queryCount->bindValue(5, $searchString);
        $queryCount->bindValue(6, $searchString);
        $queryCount->bindValue(7, $searchString);
        $queryCount->bindValue(8, $searchString);
        $queryCount->bindValue(9, $searchString);
        $queryCount->bindValue(10, $searchString);
        $queryCount->bindValue(11, $searchString);
        $queryCount->bindValue(12, $searchString);
        $queryCount->bindValue(13, $searchString);
        $queryCount->bindValue(14, $searchString);
        $queryCount->bindValue(15, $searchString);
        $queryCount->bindValue(16, $searchString);

        if (!$queryCount->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'draw' => intval($data['page']),
            'recordsTotal' => intval($queryCount->rowCount()),
            'recordsFiltered' => intval($queryCount->rowCount()),
            'doctors' => $result,
        ];
    }

    public function register($doctor, $path)
    {
        try {
            $this->db->beginTransaction();

            // Check for duplicate fields
            $tables = ['medicos', 'medicos', 'medicos', 'medicos', 'medicos', 'login'];
            $fields = ['cpf', 'rg', 'crm', 'celular', 'telefone', 'email'];
            $words = ['cpf', 'rg', 'crm', 'celular', 'telefone', 'e-mail'];

            $checkMessage = $this->utils->checkDuplicates($doctor, $tables, $fields, $words);

            if ($checkMessage) {throw new Exception('Campo(s) duplicado(s): ' . substr_replace($checkMessage, '', -2) . '.', 409);}

            // Login
            $resultLogin = $this->accessMapper->register($doctor);

            // Doctor
            $sql = 'INSERT INTO medicos(login_id, nome, imagem, data_nascimento, cpf, rg, crm, cep, estado, cidade, bairro, logradouro,
                    complemento, numero, celular, telefone, email) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $query = $this->db->prepare($sql);
            $query->bindValue(1, $resultLogin['lastInsertId']);
            $query->bindValue(2, $doctor['nome']);
            $query->bindValue(3, $path);
            $query->bindValue(4, $this->utils->formatDateBrToEn($doctor['dataNascimento']));
            $query->bindValue(5, $doctor['cpf']);
            $query->bindValue(6, $doctor['rg']);
            $query->bindValue(7, $doctor['crm']);
            $query->bindValue(8, $doctor['cep']);
            $query->bindValue(9, $doctor['estado']);
            $query->bindValue(10, $doctor['cidade']);
            $query->bindValue(11, $doctor['bairro']);
            $query->bindValue(12, $doctor['logradouro']);
            $query->bindValue(13, isset($doctor['complemento']) ? $doctor['complemento'] : null);
            $query->bindValue(14, $doctor['numero']);
            $query->bindValue(15, $doctor['celular']);
            $query->bindValue(16, isset($doctor['telefone']) ? $doctor['telefone'] : null);
            $query->bindValue(17, $doctor['email']);

            if (!$query->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

            // Specialties
            $lastInsertId = $this->db->lastInsertId();
            $sqlSpecialty = 'INSERT INTO especialidades_medicos(medicos_id, especialidades) VALUES (?, ?)';
            $querySpecialty = $this->db->prepare($sqlSpecialty);
            $querySpecialty->bindValue(1, $lastInsertId);
            $querySpecialty->bindValue(2, $doctor['especialidades']);

            if (!$querySpecialty->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

            $this->db->commit();
            return [
                'status' => 201,
                'message' => 'Médicos cadastrado com sucesso.',
            ];

        } catch (Exception $e) {
            $this->db->rollBack();
            return [
                'status' => $e->getCode(),
                'message' => $e->getMessage(),
            ];
        }
    }

    public function update($doctor, $path)
    {
        try {
            $this->db->beginTransaction();

            // Check for duplicate fields
            $tables = 'medicos';
            $fields = ['cpf', 'rg', 'crm', 'celular', 'telefone'];
            $words = ['cpf', 'rg', 'crm', 'celular', 'telefone'];

            $checkMessage = $this->utils->checkDuplicates($doctor, $tables, $fields, $words, $doctor['id']);

            if ($checkMessage) {throw new Exception('Campo(s) duplicado(s): ' . substr_replace($checkMessage, '', -2) . '.', 409);}

            // Doctor
            $sql = 'UPDATE medicos SET nome = ?, imagem = ?, data_nascimento = ?, cpf = ?, rg = ?, crm = ?, cep = ?, estado = ?,
                    cidade = ?, bairro = ?, logradouro = ?, complemento = ?, numero = ?, celular = ?, telefone = ? WHERE id = ?';
            $query = $this->db->prepare($sql);
            $query->bindValue(1, $doctor['nome']);
            $query->bindValue(2, $path);
            $query->bindValue(3, $this->utils->formatDateBrToEn($doctor['dataNascimento']));
            $query->bindValue(4, $doctor['cpf']);
            $query->bindValue(5, $doctor['rg']);
            $query->bindValue(6, $doctor['crm']);
            $query->bindValue(7, $doctor['cep']);
            $query->bindValue(8, $doctor['estado']);
            $query->bindValue(9, $doctor['cidade']);
            $query->bindValue(10, $doctor['bairro']);
            $query->bindValue(11, $doctor['logradouro']);
            $query->bindValue(12, isset($doctor['complemento']) ? $doctor['complemento'] : null);
            $query->bindValue(13, $doctor['numero']);
            $query->bindValue(14, $doctor['celular']);
            $query->bindValue(15, isset($doctor['telefone']) ? $doctor['telefone'] : null);
            $query->bindValue(16, $doctor['id']);

            if (!$query->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

            // Specialtiess
            $sqlSpecialty = 'UPDATE especialidades_medicos SET especialidades = ? WHERE medicos_id = ?';
            $querySpecialty = $this->db->prepare($sqlSpecialty);
            $querySpecialty->bindValue(1, $doctor['especialidades']);
            $querySpecialty->bindValue(2, $doctor['id']);

            if (!$querySpecialty->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

            $this->db->commit();
            return [
                'status' => 200,
                'message' => 'Médico atualizado com sucesso.',
            ];
        } catch (Exception $e) {
            $this->db->rollBack();
            return [
                'status' => $e->getCode(),
                'message' => $e->getMessage(),
            ];
        }
    }

    public function findById($id)
    {
        $sql = 'SELECT m.*, e.especialidades FROM medicos AS `m`
                JOIN especialidades_medicos AS `e` ON e.medicos_id = m.id 
                WHERE m.id = ? LIMIT 1';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        if ($query->rowCount() === 0) {
            return [
                'status' => 404,
                'message' => 'Médico não encontrado',
            ];
        }

        $result = $query->fetch(\PDO::FETCH_ASSOC);

        $obj = new \stdClass;
        $obj->id = $result['id'];
        $obj->nome = $result['nome'];
        $obj->imagemAntiga = $result['imagem'];
        $obj->dataNascimento = $result['data_nascimento'];
        $obj->especialidades = $result['especialidades'];
        $obj->cpf = $result['cpf'];
        $obj->rg = $result['rg'];
        $obj->crm = $result['crm'];
        $obj->cep = $result['cep'];
        $obj->estado = $result['estado'];
        $obj->cidade = $result['cidade'];
        $obj->bairro = $result['bairro'];
        $obj->logradouro = $result['logradouro'];
        $obj->complemento = $result['complemento'];
        $obj->numero = $result['numero'];
        $obj->celular = $result['celular'];
        $obj->telefone = $result['telefone'];

        return [
            'status' => 200,
            'message' => 'Médico encontrado com sucesso.',
            'doctor' => $obj,
        ];
    }

    public function updateStatus($id, $status)
    {
        $sql = 'UPDATE medicos SET status = ? WHERE id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $status);
        $query->bindValue(2, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Médico ' . ($status === '1' ? 'ativado' : 'desativado') . ' com sucesso.',
        ];
    }

}
