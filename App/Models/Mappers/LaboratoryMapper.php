<?php
namespace App\Models\Mappers;

use App\Core\Database;
use App\Helpers\Utils;
use App\Models\Mappers\AccessMapper;
use Exception;
use PDO as PDO;

class LaboratoryMapper
{
    /**
     * PDO Object to interact with database
     *
     * @var PDO
     */
    private $db;

    /**
     * Other variables
     */
    private $utils;
    private $accessMapper;

    /**
     * Constructor of the class
     */
    public function __construct()
    {
        // Opening database connection
        $this->db = new Database();
        $this->db = $this->db->getInstance();
        // Other variables
        $this->utils = new Utils();
        $this->accessMapper = new AccessMapper();
    }

    public function searchAll($data)
    {
        $concat = '';
        $perPage = $data['length'];
        $offset = ($perPage * $data['page']) - $perPage;
        $searchString = '%' . $data['search'] . '%';

        // Laboratories
        $sql = 'SELECT * FROM laboratorios WHERE ';
        $concat = $concat . 'nome LIKE ? OR razao_social LIKE ? OR cnpj LIKE ? OR
            cep LIKE ? OR estado LIKE ? OR cidade LIKE ? OR bairro LIKE ? OR
            logradouro LIKE ? OR complemento LIKE ? OR numero LIKE ? OR
            celular LIKE ? OR telefone LIKE ? OR email LIKE ? ';
        $concat = $concat . 'ORDER BY status DESC, id ASC ';
        $sql = $sql . $concat . 'LIMIT ? OFFSET ?';

        $query = $this->db->prepare($sql);
        $query->bindValue(1, $searchString);
        $query->bindValue(2, $searchString);
        $query->bindValue(3, $searchString);
        $query->bindValue(4, $searchString);
        $query->bindValue(5, $searchString);
        $query->bindValue(6, $searchString);
        $query->bindValue(7, $searchString);
        $query->bindValue(8, $searchString);
        $query->bindValue(9, $searchString);
        $query->bindValue(10, $searchString);
        $query->bindValue(11, $searchString);
        $query->bindValue(12, $searchString);
        $query->bindValue(13, $searchString);
        $query->bindValue(14, (int) $perPage, PDO::PARAM_INT);
        $query->bindValue(15, (int) $offset, PDO::PARAM_INT);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        // Count
        $sqlCount = 'SELECT * FROM laboratorios WHERE ';
        $sqlCount = $sqlCount . $concat;

        $queryCount = $this->db->prepare($sqlCount);
        $queryCount->bindValue(1, $searchString);
        $queryCount->bindValue(2, $searchString);
        $queryCount->bindValue(3, $searchString);
        $queryCount->bindValue(4, $searchString);
        $queryCount->bindValue(5, $searchString);
        $queryCount->bindValue(6, $searchString);
        $queryCount->bindValue(7, $searchString);
        $queryCount->bindValue(8, $searchString);
        $queryCount->bindValue(9, $searchString);
        $queryCount->bindValue(10, $searchString);
        $queryCount->bindValue(11, $searchString);
        $queryCount->bindValue(12, $searchString);
        $queryCount->bindValue(13, $searchString);

        if (!$queryCount->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'draw' => intval($data['page']),
            'recordsTotal' => intval($queryCount->rowCount()),
            'recordsFiltered' => intval($queryCount->rowCount()),
            'laboratories' => $result,
        ];
    }

    public function register($laboratory, $path)
    {
        try {
            $this->db->beginTransaction();

            // Check for duplicate fields
            $tables = ['laboratorios', 'laboratorios', 'laboratorios', 'laboratorios', 'login'];
            $fields = ['razao_social', 'cnpj', 'celular', 'telefone', 'email'];
            $words = ['razão social', 'cnpj', 'celular', 'telefone', 'e-mail'];

            $checkMessage = $this->utils->checkDuplicates($laboratory, $tables, $fields, $words);

            if ($checkMessage) {throw new Exception('Campo(s) duplicado(s): ' . substr_replace($checkMessage, '', -2) . '.', 409);}

            // Login
            $resultLogin = $this->accessMapper->register($laboratory);

            // Laboratory
            $sql = 'INSERT INTO laboratorios(login_id, nome, razao_social, imagem, cnpj, cep, estado, cidade, bairro, logradouro,
                    complemento, numero, celular, telefone, email) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $query = $this->db->prepare($sql);
            $query->bindValue(1, $resultLogin['lastInsertId']);
            $query->bindValue(2, $laboratory['nome']);
            $query->bindValue(3, $laboratory['razaoSocial']);
            $query->bindValue(4, $path);
            $query->bindValue(5, $laboratory['cnpj']);
            $query->bindValue(6, $laboratory['cep']);
            $query->bindValue(7, $laboratory['estado']);
            $query->bindValue(8, $laboratory['cidade']);
            $query->bindValue(9, $laboratory['bairro']);
            $query->bindValue(10, $laboratory['logradouro']);
            $query->bindValue(11, isset($laboratory['complemento']) ? $laboratory['complemento'] : null);
            $query->bindValue(12, $laboratory['numero']);
            $query->bindValue(13, $laboratory['celular']);
            $query->bindValue(14, isset($laboratory['telefone']) ? $laboratory['telefone'] : null);
            $query->bindValue(15, $laboratory['email']);

            if (!$query->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

            $this->db->commit();
            return [
                'status' => 201,
                'message' => 'Laboratório cadastrado com sucesso.',
            ];

        } catch (Exception $e) {
            $this->db->rollBack();
            return [
                'status' => $e->getCode(),
                'message' => $e->getMessage(),
            ];
        }
    }

    public function update($laboratory, $path)
    {
        // Check for duplicate fields
        $tables = 'laboratorios';
        $fields = ['razao_social', 'cnpj', 'celular', 'telefone'];
        $words = ['razão social', 'cnpj', 'celular', 'telefone'];

        $checkMessage = $this->utils->checkDuplicates($laboratory, $tables, $fields, $words, $laboratory['id']);

        if ($checkMessage) {
            return [
                'status' => 409,
                'message' => 'Campo(s) duplicado(s): ' . substr_replace($checkMessage, '', -2) . '.',
            ];
        }

        // Laboratory
        $sql = 'UPDATE laboratorios SET nome = ?, razao_social = ?, imagem = ?, cnpj = ?, cep = ?, estado = ?, cidade = ?,
                bairro = ?, logradouro = ?, complemento = ?, numero = ?, celular = ?, telefone = ? WHERE id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $laboratory['nome']);
        $query->bindValue(2, $laboratory['razaoSocial']);
        $query->bindValue(3, $path);
        $query->bindValue(4, $laboratory['cnpj']);
        $query->bindValue(5, $laboratory['cep']);
        $query->bindValue(6, $laboratory['estado']);
        $query->bindValue(7, $laboratory['cidade']);
        $query->bindValue(8, $laboratory['bairro']);
        $query->bindValue(9, $laboratory['logradouro']);
        $query->bindValue(10, isset($laboratory['complemento']) ? $laboratory['complemento'] : null);
        $query->bindValue(11, $laboratory['numero']);
        $query->bindValue(12, $laboratory['celular']);
        $query->bindValue(13, isset($laboratory['telefone']) ? $laboratory['telefone'] : null);
        $query->bindValue(14, $laboratory['id']);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Laboratório atualizado com sucesso.',
        ];
    }

    public function findById($id)
    {
        $sql = 'SELECT * FROM laboratorios WHERE id = ? LIMIT 1';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        if ($query->rowCount() === 0) {
            return [
                'status' => 404,
                'message' => 'Laboratório não encontrado',
            ];
        }

        $result = $query->fetch(\PDO::FETCH_ASSOC);

        $obj = new \stdClass;
        $obj->id = $result['id'];
        $obj->nome = $result['nome'];
        $obj->razaoSocial = $result['razao_social'];
        $obj->imagemAntiga = $result['imagem'];
        $obj->cnpj = $result['cnpj'];
        $obj->cep = $result['cep'];
        $obj->estado = $result['estado'];
        $obj->cidade = $result['cidade'];
        $obj->bairro = $result['bairro'];
        $obj->logradouro = $result['logradouro'];
        $obj->complemento = $result['complemento'];
        $obj->numero = $result['numero'];
        $obj->celular = $result['celular'];
        $obj->telefone = $result['telefone'];

        return [
            'status' => 200,
            'message' => 'Laboratório encontrado com sucesso.',
            'laboratory' => $obj,
        ];
    }

    public function updateStatus($id, $status)
    {
        $sql = 'UPDATE laboratorios SET status = ? WHERE id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $status);
        $query->bindValue(2, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Laboratório ' . ($status === '1' ? 'ativado' : 'desativado') . ' com sucesso.',
        ];
    }

}
