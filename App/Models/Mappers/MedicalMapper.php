<?php
namespace App\Models\Mappers;

use App\Core\Database;
use App\Helpers\Utils;
use App\Models\Mappers\AccessMapper;
use Exception;
use PDO as PDO;

class MedicalMapper
{
    /**
     * PDO Object to interact with database
     *
     * @var PDO
     */
    private $db;

    /**
     * Other variables
     */
    private $utils;
    private $accessMapper;

    /**
     * Constructor of the class
     */
    public function __construct()
    {
        // Opening database connection
        $this->db = new Database();
        $this->db = $this->db->getInstance();
        // Other variables
        $this->utils = new Utils();
        $this->accessMapper = new AccessMapper();
    }

    public function registerMedical($data)
    {
        try {
            $this->db->beginTransaction();

            $medicalRecord = $this->updateStatusSchedule($data['record']['agendamento']['id']);
            $medicalRecord = $this->registerMedicalRecord($data['record']);
            if ($data['exam']) {$exam = $this->registerExam($data['exam'], $medicalRecord['lastInsertId']);}
            if ($data['prescription']) {$prescription = $this->registerPrescription($data['prescription'], $medicalRecord['lastInsertId']);}
            if ($data['certificate']) {$certificate = $this->registerCertificate($data['certificate'], $medicalRecord['lastInsertId']);}

            $this->db->commit();
            return [
                'status' => 201,
                'message' => 'Prontuário cadastrado com sucesso.',
            ];
        } catch (Exception $e) {
            $this->db->rollBack();
            return [
                'status' => $e->getCode(),
                'message' => $e->getMessage(),
            ];
        }
    }

    public function updateStatusSchedule($scheduleId)
    {
        $sql = 'UPDATE agendamentos SET status = "1" WHERE id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $scheduleId);

        if (!$query->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

        return [
            'status' => 200,
            'message' => 'Agendamento atualizado com sucesso.',
        ];
    }

    public function registerMedicalRecord($medicalRecord)
    {
        $sql = 'INSERT INTO prontuarios(agendamentos_id, pacientes_id, medicos_id, peso, altura,
        press_arter, freq_card, freq_resp, temperatura, queixa_principal, hist_doenca_atual,
        interr_sintom, antec_pess_famil) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $medicalRecord['agendamento']['id']);
        $query->bindValue(2, $medicalRecord['paciente']['id']);
        $query->bindValue(3, $medicalRecord['medico']['id']);
        $query->bindValue(4, isset($medicalRecord['peso']) && !empty($medicalRecord['peso']) ? $medicalRecord['peso'] : null);
        $query->bindValue(5, isset($medicalRecord['altura']) && !empty($medicalRecord['altura']) ? $medicalRecord['altura'] : null);
        $query->bindValue(6, isset($medicalRecord['pressArter']) && !empty($medicalRecord['pressArter']) ? $medicalRecord['pressArter'] : null);
        $query->bindValue(7, isset($medicalRecord['freqCard']) && !empty($medicalRecord['freqCard']) ? $medicalRecord['freqCard'] : null);
        $query->bindValue(8, isset($medicalRecord['freqResp']) && !empty($medicalRecord['freqResp']) ? $medicalRecord['freqResp'] : null);
        $query->bindValue(9, isset($medicalRecord['temperatura']) && !empty($medicalRecord['temperatura']) ? $medicalRecord['temperatura'] : null);
        $query->bindValue(10, isset($medicalRecord['queixaPrincipal']) && !empty($medicalRecord['queixaPrincipal']) ? $medicalRecord['queixaPrincipal'] : null);
        $query->bindValue(11, isset($medicalRecord['histDoencaAtual']) && !empty($medicalRecord['histDoencaAtual']) ? $medicalRecord['histDoencaAtual'] : null);
        $query->bindValue(12, isset($medicalRecord['interrSintom']) && !empty($medicalRecord['interrSintom']) ? $medicalRecord['interrSintom'] : null);
        $query->bindValue(13, isset($medicalRecord['antecPessFamil']) && !empty($medicalRecord['antecPessFamil']) ? $medicalRecord['antecPessFamil'] : null);

        if (!$query->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

        return [
            'status' => 201,
            'message' => 'Prontuário cadastrado com sucesso.',
            'lastInsertId' => $this->db->lastInsertId(),
        ];
    }

    public function findRecordByScheduleId($id)
    {
        $sql = 'SELECT pr.*, a.tipo, a.status, a.data_inicio, a.data_fim, a.data_agendamento, p.nome AS `paciente_nome`,
                p.nome_mae, p.data_nascimento, p.celular, p.telefone, p.email, p.nome_responsavel, p.cpf_responsavel,
                m.nome AS `medico_nome`, m.crm
                FROM prontuarios AS `pr`
                JOIN agendamentos AS `a` ON a.id = pr.agendamentos_id
                JOIN pacientes AS `p` ON p.id = pr.pacientes_id
                JOIN medicos AS `m` ON m.id = pr.medicos_id
                WHERE pr.agendamentos_id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetch(PDO::FETCH_ASSOC);

        $obj = new \stdClass;
        $obj->id = $result['id'];
        $obj->peso = $result['peso'];
        $obj->altura = $result['altura'];
        $obj->pressArter = $result['press_arter'];
        $obj->freqCard = $result['freq_card'];
        $obj->freqResp = $result['freq_resp'];
        $obj->temperatura = $result['temperatura'];
        $obj->queixaPrincipal = $result['queixa_principal'];
        $obj->histDoencaAtual = $result['hist_doenca_atual'];
        $obj->interrSintom = $result['interr_sintom'];
        $obj->antecPessFamil = $result['antec_pess_famil'];
        $obj->agendamento['id'] = $result['agendamentos_id'];
        $obj->agendamento['tipo'] = $result['tipo'];
        $obj->agendamento['status'] = $result['status'];
        $obj->agendamento['dataInicio'] = $result['data_inicio'];
        $obj->agendamento['dataFim'] = $result['data_fim'];
        $obj->agendamento['dataAgendamento'] = $result['data_agendamento'];
        $obj->paciente['id'] = $result['pacientes_id'];
        $obj->paciente['nome'] = $result['paciente_nome'];
        $obj->paciente['nomeMae'] = $result['nome_mae'];
        $obj->paciente['dataNascimento'] = $result['data_nascimento'];
        $obj->paciente['celular'] = $result['celular'];
        $obj->paciente['telefone'] = $result['telefone'];
        $obj->paciente['email'] = $result['email'];
        $obj->paciente['nomeResponsavel'] = $result['nome_responsavel'];
        $obj->paciente['cpfResponsavel'] = $result['cpf_responsavel'];
        $obj->medico['id'] = $result['medicos_id'];
        $obj->medico['nome'] = $result['medico_nome'];
        $obj->medico['crm'] = $result['crm'];

        return [
            'status' => 200,
            'message' => 'Prontuário encontrado com sucesso.',
            'record' => $obj,
        ];
    }

    public function findRecordHistoricByPatientId($id, $page)
    {
        $perPage = 10;
        $offset = ($perPage * $page) - $perPage;

        $sql = 'SELECT pr.*, ag.data_inicio, ag.data_fim, ag.tipo,
                pa.nome AS `paciente_nome`, m.nome AS `medico_nome`, m.crm
                FROM prontuarios AS `pr`
                JOIN agendamentos AS `ag` ON ag.id = pr.agendamentos_id
                JOIN pacientes AS `pa` ON pa.id = pr.pacientes_id
                JOIN medicos AS `m` ON m.id = pr.medicos_id
                WHERE pr.pacientes_id = ?
                ORDER BY pr.id DESC';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        $arrayObj = array();
        foreach ($result as $item) {
            $obj = new \stdClass;
            $obj->id = $item['id'];
            $obj->peso = $item['peso'];
            $obj->altura = $item['altura'];
            if ($item['peso'] && $item['altura']) {
                $obj->imc = (float) number_format($item['peso'] / ($item['altura'] * $item['altura']), 2, '.', ' ');
            } else { $obj->imc = null;}
            $obj->pressArter = $item['press_arter'];
            $obj->freqCard = $item['freq_card'];
            $obj->freqResp = $item['freq_resp'];
            $obj->temperatura = $item['temperatura'];
            $obj->queixaPrincipal = $item['queixa_principal'];
            $obj->histDoencaAtual = $item['hist_doenca_atual'];
            $obj->interrSintom = $item['interr_sintom'];
            $obj->antecPessFamil = $item['antec_pess_famil'];
            $obj->agendamento['id'] = $item['agendamentos_id'];
            $obj->agendamento['dataInicio'] = $item['data_inicio'];
            $obj->agendamento['dataFim'] = $item['data_fim'];
            $obj->paciente['id'] = $item['pacientes_id'];
            $obj->paciente['nome'] = $item['paciente_nome'];
            $obj->medico['id'] = $item['medicos_id'];
            $obj->medico['nome'] = $item['medico_nome'];
            $obj->medico['crm'] = $item['crm'];
            $arrayObj[] = $obj;
        }

        return [
            'status' => 200,
            'message' => 'Prontuário encontrado com sucesso.',
            'records' => $arrayObj,
        ];
    }

    public function registerExam($exams, $medicalRecordId)
    {
        foreach ($exams as $exam) {
            $login = substr(uniqid(md5(time() . rand())), 0, 6);
            $password = $this->generateStrongPassword();

            $sql = 'INSERT INTO exames(prontuarios_id, login, senha) VALUES (?, ?, ?)';
            $query = $this->db->prepare($sql);
            $query->bindValue(1, $medicalRecordId);
            $query->bindValue(2, $login);
            $query->bindValue(3, $password);

            if (!$query->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

            $lastInsertId = $this->db->lastInsertId();

            foreach ($exam['tipoExame'] as $type) {
                $sqlType = 'INSERT INTO exames_tipos_exames(exames_id, tipos_exames_id) VALUES (?, ?)';
                $queryType = $this->db->prepare($sqlType);
                $queryType->bindValue(1, $lastInsertId);
                $queryType->bindValue(2, $type['id']);
                if (!$queryType->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}
            }
        }

        return [
            'status' => 201,
            'message' => 'Exame cadastrado com sucesso.',
        ];
    }

    public function uploadExam($id, $file)
    {
        $sql = 'UPDATE exames SET status_entrega = "1", resultado = ?, data_entrega = now() WHERE id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $file);
        $query->bindValue(2, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Resultado atualizado com sucesso.',
        ];
    }

    public function downloadExam($id)
    {
        $sql = 'SELECT e.*, ete.id AS `examesTiposExamesId`, ete.tipos_exames_id, te.nome,
                pr.agendamentos_id, pr.pacientes_id, pr.medicos_id,
                ag.data_inicio, ag.data_fim, ag.tipo, pa.nome AS `paciente_nome`,
                pa.cpf AS `paciente_cpf`, pa.celular AS `paciente_celular`,
                pa.telefone AS `paciente_telefone`, pa.nome_responsavel,
                pa.cpf_responsavel, m.nome AS `medico_nome`,
                m.crm, m.celular AS `medico_celular`, m.telefone AS `medico_telefone`
                FROM exames_tipos_exames AS `ete`
                JOIN exames AS `e` ON e.id = ete.exames_id
                JOIN tipos_exames AS `te` ON te.id = ete.tipos_exames_id
                JOIN prontuarios AS `pr` ON pr.id = e.prontuarios_id
                JOIN agendamentos AS `ag` ON ag.id = pr.agendamentos_id
                JOIN pacientes AS `pa` ON pa.id = pr.pacientes_id
                JOIN medicos AS `m` ON m.id = pr.medicos_id
                WHERE e.id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        $arrayObj = array();
        foreach ($result as $index => $item) {
            if (count($arrayObj) && $arrayObj[$indexRecord]->prontuario['id'] === $item['prontuarios_id']) {
                if ($arrayObj[$indexRecord]->exame->id === $item['id']) {
                    $objType = new \stdClass;
                    $objType->id = $item['tipos_exames_id'];
                    $objType->nome = $item['nome'];
                    $arrayObj[$indexRecord]->exame->tipoExame[] = $objType;
                } else {
                    $objExam = new \stdClass;
                    $objExam->id = $item['id'];
                    $objExam->login = $item['login'];
                    $objExam->senha = $item['senha'];
                    $objExam->resultado = $item['resultado'];
                    $objExam->statusEntrega = $item['status_entrega'];
                    $objExam->dataEntrega = $item['data_entrega'];
                    $objExam->dataSolicitacao = $item['data_solicitacao'];
                    $obj->exame = $objExam;

                    $objType = new \stdClass;
                    $objType->id = $item['tipos_exames_id'];
                    $objType->nome = $item['nome'];
                    $arrayObj[$indexRecord]->exame->tipoExame[] = $objType;
                }
            } else {
                $indexRecord = $index;
                $obj = new \stdClass;
                $obj->prontuario['id'] = $item['prontuarios_id'];
                $obj->prontuario['agendamento']['id'] = $item['agendamentos_id'];
                $obj->prontuario['agendamento']['dataInicio'] = $item['data_inicio'];
                $obj->prontuario['agendamento']['dataFim'] = $item['data_fim'];
                $obj->prontuario['paciente']['id'] = $item['pacientes_id'];
                $obj->prontuario['paciente']['nome'] = $item['paciente_nome'];
                $obj->prontuario['paciente']['cpf'] = $item['paciente_cpf'];
                $obj->prontuario['paciente']['celular'] = $item['paciente_celular'];
                $obj->prontuario['paciente']['telefone'] = $item['paciente_telefone'];
                $obj->prontuario['paciente']['nomeResponsavel'] = $item['nome_responsavel'];
                $obj->prontuario['paciente']['cpfResponsavel'] = $item['cpf_responsavel'];
                $obj->prontuario['medico']['id'] = $item['medicos_id'];
                $obj->prontuario['medico']['nome'] = $item['medico_nome'];
                $obj->prontuario['medico']['crm'] = $item['crm'];
                $obj->prontuario['medico']['celular'] = $item['medico_celular'];
                $obj->prontuario['medico']['telefone'] = $item['medico_telefone'];

                $objExam = new \stdClass;
                $objExam->id = $item['id'];
                $objExam->resultado = $item['resultado'];
                $objExam->statusEntrega = $item['status_entrega'];
                $objExam->dataEntrega = $item['data_entrega'];
                $objExam->dataSolicitacao = $item['data_solicitacao'];
                $obj->exame = $objExam;

                $objType = new \stdClass;
                $objType->id = $item['tipos_exames_id'];
                $objType->nome = $item['nome'];
                $obj->exame->tipoExame[] = $objType;

                $arrayObj[] = $obj;
            }
        }

        return [
            'status' => 200,
            'message' => 'Exame encontrado com sucesso.',
            'exams' => $arrayObj,
        ];
    }

    public function findExamByRecordId($id)
    {
        $sql = 'SELECT e.*, ete.id AS `examesTiposExamesId`, ete.tipos_exames_id, te.nome
                FROM exames_tipos_exames AS `ete`
                JOIN exames AS `e` ON e.id = ete.exames_id
                JOIN tipos_exames AS `te` ON te.id = ete.tipos_exames_id
                WHERE e.prontuarios_id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        $arrayObj = array();
        foreach ($result as $index => $item) {
            if (count($arrayObj) && $arrayObj[$indexExam]->exame['id'] === $item['id']) {
                $objType = new \stdClass;
                $objType->id = $item['tipos_exames_id'];
                $objType->nome = $item['nome'];
                $arrayObj[$indexExam]->tipoExame[] = $objType;
            } else {
                $indexExam = $index;
                $obj = new \stdClass;
                $obj->id = $item['examesTiposExamesId'];
                $obj->exame['id'] = $item['id'];
                $obj->exame['login'] = $item['login'];
                $obj->exame['senha'] = $item['senha'];
                $obj->exame['resultado'] = $item['resultado'];
                $obj->exame['statusEntrega'] = $item['status_entrega'];
                $obj->exame['dataEntrega'] = $item['data_entrega'];
                $obj->exame['dataSolicitacao'] = $item['data_solicitacao'];

                $objType = new \stdClass;
                $objType->id = $item['tipos_exames_id'];
                $objType->nome = $item['nome'];
                $obj->tipoExame[] = $objType;

                $arrayObj[] = $obj;
            }
        }

        return [
            'status' => 200,
            'message' => 'Exame encontrado com sucesso.',
            'exams' => $arrayObj,
        ];
    }

    public function findExamHistoricByPatientId($id, $page)
    {
        $perPage = 10;
        $offset = ($perPage * $page) - $perPage;

        $sql = 'SELECT e.*, ete.id AS `examesTiposExamesId`, ete.tipos_exames_id, te.nome,
                pr.agendamentos_id, pr.pacientes_id, pr.medicos_id,
                ag.data_inicio, ag.data_fim, ag.tipo, pa.nome AS `paciente_nome`,
                m.nome AS `medico_nome`, m.crm
                FROM exames_tipos_exames AS `ete`
                JOIN exames AS `e` ON e.id = ete.exames_id
                JOIN tipos_exames AS `te` ON te.id = ete.tipos_exames_id
                JOIN prontuarios AS `pr` ON pr.id = e.prontuarios_id
                JOIN agendamentos AS `ag` ON ag.id = pr.agendamentos_id
                JOIN pacientes AS `pa` ON pa.id = pr.pacientes_id
                JOIN medicos AS `m` ON m.id = pr.medicos_id
                WHERE pr.pacientes_id = ?
                ORDER BY pr.id DESC, e.id ASC';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        $arrayObj = array();
        foreach ($result as $index => $item) {
            if (count($arrayObj)) {
                $indexRecord = count($arrayObj) - 1;
                $indexExams = count($arrayObj[$indexRecord]->exames) - 1;
                $indexType = count($arrayObj[$indexRecord]->exames[$indexExams]->tipoExame) - 1;
            }

            if (count($arrayObj) && $arrayObj[$indexRecord]->prontuario['id'] === $item['prontuarios_id']) {
                if ($arrayObj[$indexRecord]->exames[$indexExams]->exame['id'] === $item['id']) {
                    $objType = new \stdClass;
                    $objType->id = $item['tipos_exames_id'];
                    $objType->nome = $item['nome'];
                    $arrayObj[$indexRecord]->exames[$indexExams]->tipoExame[$indexType + 1] = $objType;
                } else {
                    $objExam = new \stdClass;
                    $objExam->id = $item['examesTiposExamesId'];
                    $objExam->exame['id'] = $item['id'];
                    $objExam->exame['login'] = $item['login'];
                    $objExam->exame['senha'] = $item['senha'];
                    $objExam->exame['resultado'] = $item['resultado'];
                    $objExam->exame['statusEntrega'] = $item['status_entrega'];
                    $objExam->exame['dataEntrega'] = $item['data_entrega'];
                    $objExam->exame['dataSolicitacao'] = $item['data_solicitacao'];
                    $arrayObj[$indexRecord]->exames[$indexExams + 1] = $objExam;

                    $objType = new \stdClass;
                    $objType->id = $item['tipos_exames_id'];
                    $objType->nome = $item['nome'];
                    $arrayObj[$indexRecord]->exames[$indexExams + 1]->tipoExame[] = $objType;
                }
            } else {
                $obj = new \stdClass;
                $obj->prontuario['id'] = $item['prontuarios_id'];
                $obj->prontuario['agendamento']['id'] = $item['agendamentos_id'];
                $obj->prontuario['agendamento']['dataInicio'] = $item['data_inicio'];
                $obj->prontuario['agendamento']['dataFim'] = $item['data_fim'];
                $obj->prontuario['paciente']['id'] = $item['pacientes_id'];
                $obj->prontuario['paciente']['nome'] = $item['paciente_nome'];
                $obj->prontuario['medico']['id'] = $item['medicos_id'];
                $obj->prontuario['medico']['nome'] = $item['medico_nome'];
                $obj->prontuario['medico']['crm'] = $item['crm'];

                $objExam = new \stdClass;
                $objExam->id = $item['examesTiposExamesId'];
                $objExam->exame['id'] = $item['id'];
                $objExam->exame['login'] = $item['login'];
                $objExam->exame['senha'] = $item['senha'];
                $objExam->exame['resultado'] = $item['resultado'];
                $objExam->exame['statusEntrega'] = $item['status_entrega'];
                $objExam->exame['dataEntrega'] = $item['data_entrega'];
                $objExam->exame['dataSolicitacao'] = $item['data_solicitacao'];
                $obj->exames[] = $objExam;

                $objType = new \stdClass;
                $objType->id = $item['tipos_exames_id'];
                $objType->nome = $item['nome'];
                $obj->exames[0]->tipoExame[] = $objType;

                $arrayObj[] = $obj;
            }
        }

        return [
            'status' => 200,
            'message' => 'Exame encontrado com sucesso.',
            'items' => $arrayObj,
        ];
    }

    public function registerPrescription($prescriptions, $medicalRecordId)
    {
        foreach ($prescriptions as $prescription) {
            $sql = 'INSERT INTO prescricoes(prontuarios_id, medicamentos_id, posologia) VALUES (?, ?, ?)';
            $query = $this->db->prepare($sql);
            $query->bindValue(1, $medicalRecordId);
            $query->bindValue(2, $prescription['medicamento']['id']);
            $query->bindValue(3, $prescription['posologia']);

            if (!$query->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}
        }

        return [
            'status' => 201,
            'message' => 'Prescrição cadastrado com sucesso.',
        ];
    }

    public function findPrescriptionByRecordId($id)
    {
        $sql = 'SELECT p.*, m.nome_generico, m.nome_fabrica, m.nome_fabricante
                FROM prescricoes AS `p`
                JOIN medicamentos AS `m` ON m.id = p.medicamentos_id
                WHERE prontuarios_id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        $arrayObj = array();
        foreach ($result as $item) {
            $obj = new \stdClass;
            $obj->id = $item['id'];
            $obj->posologia = $item['posologia'];
            $obj->prontuario['id'] = $item['prontuarios_id'];
            $obj->medicamento['id'] = $item['medicamentos_id'];
            $obj->medicamento['nomeGenerico'] = $item['nome_generico'];
            $obj->medicamento['nomeFabrica'] = $item['nome_fabrica'];
            $obj->medicamento['nomeFabricante'] = $item['nome_fabricante'];
            $arrayObj[] = $obj;
        }

        return [
            'status' => 200,
            'message' => 'Prescrição encontrada com sucesso.',
            'prescriptions' => $arrayObj,
        ];
    }

    public function findPrescriptionHistoricByPatientId($id, $page)
    {
        $perPage = 10;
        $offset = ($perPage * $page) - $perPage;

        $sql = 'SELECT p.*, me.id AS `medicamento_id`, me.nome_generico, me.nome_fabrica,
                me.nome_fabricante, pr.agendamentos_id, pr.pacientes_id, pr.medicos_id,
                ag.data_inicio, ag.data_fim, ag.tipo, pa.nome AS `paciente_nome`,
                m.nome AS `medico_nome`, m.crm
                FROM prescricoes AS `p`
                JOIN medicamentos AS `me` ON me.id = p.medicamentos_id
                JOIN prontuarios AS `pr` ON pr.id = p.prontuarios_id
                JOIN agendamentos AS `ag` ON ag.id = pr.agendamentos_id
                JOIN pacientes AS `pa` ON pa.id = pr.pacientes_id
                JOIN medicos AS `m` ON m.id = pr.medicos_id
                WHERE pr.pacientes_id = ?
                ORDER BY pr.id DESC, p.id ASC';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        $arrayObj = array();
        foreach ($result as $index => $item) {
            if (count($arrayObj) && $arrayObj[$indexRecord]->prontuario['id'] === $item['prontuarios_id']) {
                $objMedicament = new \stdClass;
                $objMedicament->id = $item['id'];
                $objMedicament->posologia = $item['posologia'];
                $objMedicament->medicamento['id'] = $item['medicamento_id'];
                $objMedicament->medicamento['nomeGenerico'] = $item['nome_generico'];
                $objMedicament->medicamento['nomeFabrica'] = $item['nome_fabrica'];
                $objMedicament->medicamento['nomeFabricante'] = $item['nome_fabricante'];
                $arrayObj[$indexRecord]->prescricao[] = $objMedicament;
            } else {
                $indexRecord = $index;
                $obj = new \stdClass;
                $obj->prontuario['id'] = $item['prontuarios_id'];
                $obj->prontuario['agendamento']['id'] = $item['agendamentos_id'];
                $obj->prontuario['agendamento']['dataInicio'] = $item['data_inicio'];
                $obj->prontuario['agendamento']['dataFim'] = $item['data_fim'];
                $obj->prontuario['paciente']['id'] = $item['pacientes_id'];
                $obj->prontuario['paciente']['nome'] = $item['paciente_nome'];
                $obj->prontuario['medico']['id'] = $item['medicos_id'];
                $obj->prontuario['medico']['nome'] = $item['medico_nome'];
                $obj->prontuario['medico']['crm'] = $item['crm'];

                $objMedicament = new \stdClass;
                $objMedicament->id = $item['id'];
                $objMedicament->posologia = $item['posologia'];
                $objMedicament->medicamento['id'] = $item['medicamento_id'];
                $objMedicament->medicamento['nomeGenerico'] = $item['nome_generico'];
                $objMedicament->medicamento['nomeFabrica'] = $item['nome_fabrica'];
                $objMedicament->medicamento['nomeFabricante'] = $item['nome_fabricante'];
                $obj->prescricao[] = $objMedicament;

                $arrayObj[] = $obj;
            }
        }

        return [
            'status' => 200,
            'message' => 'Exame encontrado com sucesso.',
            'items' => $arrayObj,
        ];
    }

    public function registerCertificate($certificate, $medicalRecordId)
    {
        $sql = 'INSERT INTO atestados(prontuarios_id, periodo) VALUES (?, ?)';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $medicalRecordId);
        $query->bindValue(2, $certificate['periodo']);

        if (!$query->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

        return [
            'status' => 201,
            'message' => 'Atestado cadastrado com sucesso.',
        ];
    }

    public function findCertificateByRecordId($id)
    {
        $sql = 'SELECT id, prontuarios_id AS `prontuariosId`, periodo FROM atestados WHERE prontuarios_id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetch(PDO::FETCH_ASSOC);

        return [
            'status' => 200,
            'message' => 'Atestado encontrado com sucesso.',
            'certificate' => $result,
        ];
    }

    public function findCertificateHistoricByPatientId($id, $page)
    {
        $perPage = 10;
        $offset = ($perPage * $page) - $perPage;

        $sql = 'SELECT a.*, pr.agendamentos_id, pr.pacientes_id, pr.medicos_id,
                ag.data_inicio, ag.data_fim, ag.tipo, pa.nome AS `paciente_nome`,
                m.nome AS `medico_nome`, m.crm
                FROM atestados AS `a`
                JOIN prontuarios AS `pr` ON pr.id = a.prontuarios_id
                JOIN agendamentos AS `ag` ON ag.id = pr.agendamentos_id
                JOIN pacientes AS `pa` ON pa.id = pr.pacientes_id
                JOIN medicos AS `m` ON m.id = pr.medicos_id
                WHERE pr.pacientes_id = ?
                ORDER BY pr.id DESC';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        $arrayObj = array();
        foreach ($result as $item) {
            $obj = new \stdClass;
            $obj->id = $item['id'];
            $obj->periodo = $item['periodo'];
            $obj->prontuario['id'] = $item['prontuarios_id'];
            $obj->prontuario['agendamento']['id'] = $item['agendamentos_id'];
            $obj->prontuario['agendamento']['dataInicio'] = $item['data_inicio'];
            $obj->prontuario['agendamento']['dataFim'] = $item['data_fim'];
            $obj->prontuario['paciente']['id'] = $item['pacientes_id'];
            $obj->prontuario['paciente']['nome'] = $item['paciente_nome'];
            $obj->prontuario['medico']['id'] = $item['medicos_id'];
            $obj->prontuario['medico']['nome'] = $item['medico_nome'];
            $obj->prontuario['medico']['crm'] = $item['crm'];
            $arrayObj[] = $obj;
        }

        return [
            'status' => 200,
            'message' => 'Atestado encontrado com sucesso.',
            'certificates' => $arrayObj,
        ];
    }

    public function searchExamTypes($search)
    {
        $searchString = '%' . $search . '%';

        $sql = 'SELECT * FROM tipos_exames WHERE (nome LIKE ?)';

        $query = $this->db->prepare($sql);
        $query->bindValue(1, $searchString);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return [
            'status' => 200,
            'exams' => $result,
        ];
    }

    public function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }

        if (strpos($available_sets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }

        if (strpos($available_sets, 'd') !== false) {
            $sets[] = '23456789';
        }

        if (strpos($available_sets, 's') !== false) {
            $sets[] = '!@#$%&*?';
        }

        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }

        $password = str_shuffle($password);

        if (!$add_dashes) {
            return $password;
        }

        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;

        return $dash_str;
    }

}
