<?php
namespace App\Models\Mappers;

use App\Core\Database;
use PDO as PDO;

class MedicamentMapper
{
    /**
     * PDO Object to interact with database
     *
     * @var PDO
     */
    private $db;

    /**
     * Constructor of the class
     */
    public function __construct()
    {
        // Opening database connection
        $this->db = new Database();
        $this->db = $this->db->getInstance();
    }

    public function searchAll($data)
    {
        $concat = '';
        $perPage = $data['length'];
        $offset = ($perPage * $data['page']) - $perPage;
        $searchString = '%' . $data['search'] . '%';

        // Medicaments
        $sql = 'SELECT * FROM medicamentos WHERE ';
        $concat = $concat . 'nome_generico LIKE ? OR nome_fabrica LIKE ? OR nome_fabricante LIKE ? ';
        $concat = $concat . 'ORDER BY status DESC, id ASC ';
        $sql = $sql . $concat . 'LIMIT ? OFFSET ?';

        $query = $this->db->prepare($sql);
        $query->bindValue(1, $searchString);
        $query->bindValue(2, $searchString);
        $query->bindValue(3, $searchString);
        $query->bindValue(4, (int) $perPage, PDO::PARAM_INT);
        $query->bindValue(5, (int) $offset, PDO::PARAM_INT);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        // Count
        $sqlCount = 'SELECT * FROM medicamentos WHERE ';
        $sqlCount = $sqlCount . $concat;

        $queryCount = $this->db->prepare($sqlCount);
        $queryCount->bindValue(1, $searchString);
        $queryCount->bindValue(2, $searchString);
        $queryCount->bindValue(3, $searchString);

        if (!$queryCount->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'draw' => intval($data['page']),
            'recordsTotal' => intval($queryCount->rowCount()),
            'recordsFiltered' => intval($queryCount->rowCount()),
            'medicaments' => $result,
        ];
    }

    public function register($medicament)
    {
        $sql = 'INSERT INTO medicamentos(nome_generico, nome_fabrica, nome_fabricante) VALUES (?, ?, ?)';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, isset($medicament['nomeGenerico']) && !empty($medicament['nomeGenerico']) ? $medicament['nomeGenerico'] : null);
        $query->bindValue(2, isset($medicament['nomeFabrica']) && !empty($medicament['nomeFabrica']) ? $medicament['nomeFabrica'] : null);
        $query->bindValue(3, isset($medicament['nomeFabricante']) && !empty($medicament['nomeFabricante']) ? $medicament['nomeFabricante'] : null);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 201,
            'message' => 'Medicamentos cadastrado com sucesso.',
        ];
    }

    public function update($id, $medicament)
    {
        $sql = 'UPDATE medicamentos SET nome_generico = ?, nome_fabrica = ?, nome_fabricante = ? WHERE id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, isset($medicament['nomeGenerico']) && !empty($medicament['nomeGenerico']) ? $medicament['nomeGenerico'] : null);
        $query->bindValue(2, isset($medicament['nomeFabrica']) && !empty($medicament['nomeFabrica']) ? $medicament['nomeFabrica'] : null);
        $query->bindValue(3, isset($medicament['nomeFabricante']) && !empty($medicament['nomeFabricante']) ? $medicament['nomeFabricante'] : null);
        $query->bindValue(4, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Medicamento atualizado com sucesso.',
        ];
    }

    public function findById($id)
    {
        $sql = 'SELECT * FROM medicamentos WHERE id = ? LIMIT 1';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        if ($query->rowCount() === 0) {
            return [
                'status' => 404,
                'message' => 'Medicamento não encontrado',
            ];
        }

        $result = $query->fetch(\PDO::FETCH_ASSOC);

        $obj = new \stdClass;
        $obj->id = $result['id'];
        $obj->nomeGenerico = $result['nome_generico'];
        $obj->nomeFabrica = $result['nome_fabrica'];
        $obj->nomeFabricante = $result['nome_fabricante'];

        return [
            'status' => 200,
            'message' => 'Medicamento encontrado com sucesso.',
            'medicament' => $obj,
        ];
    }

    public function search($search)
    {
        $searchString = '%' . $search . '%';

        $sql = 'SELECT * FROM medicamentos WHERE (nome_generico LIKE ? OR nome_fabrica LIKE ?) AND status = "1"';

        $query = $this->db->prepare($sql);
        $query->bindValue(1, $searchString);
        $query->bindValue(2, $searchString);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return [
            'status' => 200,
            'medicaments' => $result,
        ];
    }

    public function updateStatus($id, $status)
    {
        $sql = 'UPDATE medicamentos SET status = ? WHERE id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $status);
        $query->bindValue(2, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Medicamento ' . ($status === '1' ? 'ativado' : 'desativado') . ' com sucesso.',
        ];
    }

}
