<?php
namespace App\Models\Mappers;

use App\Core\Database;
use App\Helpers\Utils;
use Exception;
use PDO as PDO;

class PatientMapper
{
    /**
     * PDO Object to interact with database
     *
     * @var PDO
     */
    private $db;

    /**
     * Other variables
     */
    private $utils;

    /**
     * Constructor of the class
     */
    public function __construct()
    {
        // Opening database connection
        $this->db = new Database();
        $this->db = $this->db->getInstance();
        // Other variables
        $this->utils = new Utils();
    }

    public function searchAll($data)
    {
        $concat = '';
        $perPage = $data['length'];
        $offset = ($perPage * $data['page']) - $perPage;
        $searchString = '%' . $data['search'] . '%';
        $searchData = '';

        if ($this->utils->isDate($data['search'])) {
            $splitTimestamp = explode(' ', $data['search']);
            $format = date('Y-m-d', strtotime(str_replace('/', '-', $splitTimestamp[0])));
            $searchData = '%' . $format . (isset($splitTimestamp[1]) ? ' ' . $splitTimestamp[1] : '') . '%';
        }

        // Patients
        $sql = 'SELECT * FROM pacientes WHERE ';
        $concat = $concat . 'nome LIKE ? OR nome_mae LIKE ? OR data_nascimento LIKE ? OR
            sexo LIKE ? OR tipo_sanguineo LIKE ? OR cpf LIKE ? OR rg LIKE ? OR
            cns LIKE ? OR cep LIKE ? OR estado LIKE ? OR cidade LIKE ? OR
            bairro LIKE ? OR logradouro LIKE ? OR complemento LIKE ? OR
            numero LIKE ? OR celular LIKE ? OR telefone LIKE ? OR email LIKE ? ';
        $concat = $concat . 'ORDER BY status DESC, id ASC ';
        $sql = $sql . $concat . 'LIMIT ? OFFSET ?';

        $query = $this->db->prepare($sql);
        $query->bindValue(1, $searchString);
        $query->bindValue(2, $searchString);
        $query->bindValue(3, $searchData);
        $query->bindValue(4, $searchString);
        $query->bindValue(5, $searchString);
        $query->bindValue(6, $searchString);
        $query->bindValue(7, $searchString);
        $query->bindValue(8, $searchString);
        $query->bindValue(9, $searchString);
        $query->bindValue(10, $searchString);
        $query->bindValue(11, $searchString);
        $query->bindValue(12, $searchString);
        $query->bindValue(13, $searchString);
        $query->bindValue(14, $searchString);
        $query->bindValue(15, $searchString);
        $query->bindValue(16, $searchString);
        $query->bindValue(17, $searchString);
        $query->bindValue(18, $searchString);
        $query->bindValue(19, (int) $perPage, PDO::PARAM_INT);
        $query->bindValue(20, (int) $offset, PDO::PARAM_INT);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        // Count
        $sqlCount = 'SELECT * FROM pacientes WHERE ';
        $sqlCount = $sqlCount . $concat;

        $queryCount = $this->db->prepare($sqlCount);
        $queryCount->bindValue(1, $searchString);
        $queryCount->bindValue(2, $searchString);
        $queryCount->bindValue(3, $searchData);
        $queryCount->bindValue(4, $searchString);
        $queryCount->bindValue(5, $searchString);
        $queryCount->bindValue(6, $searchString);
        $queryCount->bindValue(7, $searchString);
        $queryCount->bindValue(8, $searchString);
        $queryCount->bindValue(9, $searchString);
        $queryCount->bindValue(10, $searchString);
        $queryCount->bindValue(11, $searchString);
        $queryCount->bindValue(12, $searchString);
        $queryCount->bindValue(13, $searchString);
        $queryCount->bindValue(14, $searchString);
        $queryCount->bindValue(15, $searchString);
        $queryCount->bindValue(16, $searchString);
        $queryCount->bindValue(17, $searchString);
        $queryCount->bindValue(18, $searchString);

        if (!$queryCount->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'draw' => intval($data['page']),
            'recordsTotal' => intval($queryCount->rowCount()),
            'recordsFiltered' => intval($queryCount->rowCount()),
            'patients' => $result,
        ];
    }

    public function register($patient, $path)
    {
        try {
            $this->db->beginTransaction();

            // Check for duplicate fields
            $tables = 'pacientes';
            $fields = ['cpf', 'rg', 'cns', 'celular', 'telefone', 'email'];
            $words = ['cpf', 'rg', 'cns', 'celular', 'telefone', 'e-mail'];

            $checkMessage = $this->utils->checkDuplicates($patient, $tables, $fields, $words);

            if ($checkMessage) {throw new Exception('Campo(s) duplicado(s): ' . substr_replace($checkMessage, '', -2) . '.', 409);}

            // Patient
            $sql = 'INSERT INTO pacientes(nome, nome_mae, imagem, sexo, tipo_sanguineo, data_nascimento,
                    cpf, rg, cns, cep, estado, cidade, bairro, logradouro, complemento, numero, celular,
                    telefone, email, nome_responsavel, cpf_responsavel) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $query = $this->db->prepare($sql);
            $query->bindValue(1, $patient['nome']);
            $query->bindValue(2, $patient['nomeMae']);
            $query->bindValue(3, $path);
            $query->bindValue(4, isset($patient['sexo']) ? $patient['sexo'] : null);
            $query->bindValue(5, isset($patient['tipoSanguineo']) ? $patient['tipoSanguineo'] : null);
            $query->bindValue(6, isset($patient['dataNascimento']) ? $this->utils->formatDateBrToEn($patient['dataNascimento']) : null);
            $query->bindValue(7, isset($patient['cpf']) ? $patient['cpf'] : null);
            $query->bindValue(8, isset($patient['rg']) ? $patient['rg'] : null);
            $query->bindValue(9, isset($patient['cns']) ? $patient['cns'] : null);
            $query->bindValue(10, isset($patient['cep']) ? $patient['cep'] : null);
            $query->bindValue(11, isset($patient['estado']) ? $patient['estado'] : null);
            $query->bindValue(12, isset($patient['cidade']) ? $patient['cidade'] : null);
            $query->bindValue(13, isset($patient['bairro']) ? $patient['bairro'] : null);
            $query->bindValue(14, isset($patient['logradouro']) ? $patient['logradouro'] : null);
            $query->bindValue(15, isset($patient['complemento']) ? $patient['complemento'] : null);
            $query->bindValue(16, isset($patient['numero']) ? $patient['numero'] : null);
            $query->bindValue(17, isset($patient['celular']) ? $patient['celular'] : null);
            $query->bindValue(18, isset($patient['telefone']) ? $patient['telefone'] : null);
            $query->bindValue(19, isset($patient['email']) ? $patient['email'] : null);
            $query->bindValue(20, isset($patient['nomeResponsavel']) ? $patient['nomeResponsavel'] : null);
            $query->bindValue(21, isset($patient['cpfResponsavel']) ? $patient['cpfResponsavel'] : null);

            if (!$query->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

            $this->db->commit();
            return [
                'status' => 201,
                'message' => 'Paciente cadastrado com sucesso.',
            ];

        } catch (Exception $e) {
            $this->db->rollBack();
            return [
                'status' => $e->getCode(),
                'message' => $e->getMessage(),
            ];
        }
    }

    public function update($patient, $path)
    {
        try {
            $this->db->beginTransaction();

            // Check for duplicate fields
            $tables = 'pacientes';
            $fields = ['cpf', 'rg', 'cns', 'celular', 'telefone', 'email'];
            $words = ['cpf', 'rg', 'cns', 'celular', 'telefone', 'e-mail'];

            $checkMessage = $this->utils->checkDuplicates($patient, $tables, $fields, $words, $patient['id']);

            if ($checkMessage) {throw new Exception('Campo(s) duplicado(s): ' . substr_replace($checkMessage, '', -2) . '.', 409);}

            // Patient
            $sql = 'UPDATE pacientes SET nome = ?, imagem = ?, sexo = ?, tipo_sanguineo = ?, data_nascimento = ?,
                    cpf = ?, rg = ?, cns = ?, cep = ?, estado = ?, cidade = ?, bairro = ?, logradouro = ?,
                    complemento = ?, numero = ?, celular = ?, telefone = ?, email = ? WHERE id = ?';
            $query = $this->db->prepare($sql);
            $query->bindValue(1, $patient['nome']);
            $query->bindValue(2, $patient['nomeMae']);
            $query->bindValue(3, $path);
            $query->bindValue(4, $patient['sexo']);
            $query->bindValue(5, isset($patient['tipoSanguineo']) ? $patient['tipoSanguineo'] : null);
            $query->bindValue(6, $this->utils->formatDateBrToEn($patient['dataNascimento']));
            $query->bindValue(7, $patient['cpf']);
            $query->bindValue(8, $patient['rg']);
            $query->bindValue(9, $patient['cns']);
            $query->bindValue(10, $patient['cep']);
            $query->bindValue(11, $patient['estado']);
            $query->bindValue(12, $patient['cidade']);
            $query->bindValue(13, $patient['bairro']);
            $query->bindValue(14, $patient['logradouro']);
            $query->bindValue(15, isset($patient['complemento']) ? $patient['complemento'] : null);
            $query->bindValue(16, $patient['numero']);
            $query->bindValue(17, $patient['celular']);
            $query->bindValue(18, isset($patient['telefone']) ? $patient['telefone'] : null);
            $query->bindValue(19, isset($patient['email']) ? $patient['email'] : null);
            $query->bindValue(20, isset($patient['nomeResponsavel']) ? $patient['nomeResponsavel'] : null);
            $query->bindValue(21, isset($patient['cpfResponsavel']) ? $patient['cpfResponsavel'] : null);
            $query->bindValue(22, $patient['id']);

            if (!$query->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

            $this->db->commit();
            return [
                'status' => 200,
                'message' => 'Paciente atualizado com sucesso.',
            ];
        } catch (Exception $e) {
            $this->db->rollBack();
            return [
                'status' => $e->getCode(),
                'message' => $e->getMessage(),
            ];
        }
    }

    public function findById($id)
    {
        $sql = 'SELECT * FROM pacientes WHERE id = ? LIMIT 1';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        if ($query->rowCount() === 0) {
            return [
                'status' => 404,
                'message' => 'Paciente não encontrado',
            ];
        }

        $result = $query->fetch(\PDO::FETCH_ASSOC);

        $obj = new \stdClass;
        $obj->id = $result['id'];
        $obj->nome = $result['nome'];
        $obj->nomeMae = $result['nome_mae'];
        $obj->imagemAntiga = $result['imagem'];
        $obj->sexo = $result['sexo'];
        $obj->tipoSanguineo = $result['tipo_sanguineo'];
        $obj->dataNascimento = $result['data_nascimento'];
        $obj->cpf = $result['cpf'];
        $obj->rg = $result['rg'];
        $obj->cns = $result['cns'];
        $obj->cep = $result['cep'];
        $obj->estado = $result['estado'];
        $obj->cidade = $result['cidade'];
        $obj->bairro = $result['bairro'];
        $obj->logradouro = $result['logradouro'];
        $obj->complemento = $result['complemento'];
        $obj->numero = $result['numero'];
        $obj->celular = $result['celular'];
        $obj->telefone = $result['telefone'];
        $obj->email = $result['email'];
        $obj->nomeResponsavel = $result['nome_responsavel'];
        $obj->cpfResponsavel = $result['cpf_responsavel'];

        return [
            'status' => 200,
            'message' => 'Paciente encontrado com sucesso.',
            'patient' => $obj,
        ];
    }

    public function search($search)
    {
        $searchString = '%' . $search . '%';

        $sql = 'SELECT * FROM pacientes WHERE (nome LIKE ? OR cpf LIKE ?) AND status = "1"';

        $query = $this->db->prepare($sql);
        $query->bindValue(1, $searchString);
        $query->bindValue(2, $searchString);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return [
            'status' => 200,
            'patients' => $result,
        ];
    }

    public function updateStatus($id, $status)
    {
        $sql = 'UPDATE pacientes SET status = ? WHERE id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $status);
        $query->bindValue(2, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Paciente ' . ($status === '1' ? 'ativado' : 'desativado') . ' com sucesso.',
        ];
    }

}
