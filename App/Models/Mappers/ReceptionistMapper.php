<?php
namespace App\Models\Mappers;

use App\Core\Database;
use App\Helpers\Utils;
use App\Models\Mappers\AccessMapper;
use Exception;
use PDO as PDO;

class ReceptionistMapper
{
    /**
     * PDO Object to interact with database
     *
     * @var PDO
     */
    private $db;

    /**
     * Other variables
     */
    private $utils;
    private $accessMapper;

    /**
     * Constructor of the class
     */
    public function __construct()
    {
        // Opening database connection
        $this->db = new Database();
        $this->db = $this->db->getInstance();
        // Other variables
        $this->utils = new Utils();
        $this->accessMapper = new AccessMapper();
    }

    public function searchAll($data)
    {
        $concat = '';
        $perPage = $data['length'];
        $offset = ($perPage * $data['page']) - $perPage;
        $searchString = '%' . $data['search'] . '%';
        $searchData = '';

        if ($this->utils->isDate($data['search'])) {
            $splitTimestamp = explode(' ', $data['search']);
            $format = date('Y-m-d', strtotime(str_replace('/', '-', $splitTimestamp[0])));
            $searchData = '%' . $format . (isset($splitTimestamp[1]) ? ' ' . $splitTimestamp[1] : '') . '%';
        }

        // Receptionists
        $sql = 'SELECT * FROM recepcionistas WHERE ';
        $concat = $concat . 'nome LIKE ? OR data_nascimento LIKE ? OR
            cpf LIKE ? OR rg LIKE ? OR cep LIKE ? OR estado LIKE ? OR
            cidade LIKE ? OR bairro LIKE ? OR logradouro LIKE ? OR
            complemento LIKE ? OR numero LIKE ? OR celular LIKE ? OR
            telefone LIKE ? OR email LIKE ? ';
        $concat = $concat . 'ORDER BY status DESC, id ASC ';
        $sql = $sql . $concat . 'LIMIT ? OFFSET ?';

        $query = $this->db->prepare($sql);
        $query->bindValue(1, $searchString);
        $query->bindValue(2, $searchData);
        $query->bindValue(3, $searchString);
        $query->bindValue(4, $searchString);
        $query->bindValue(5, $searchString);
        $query->bindValue(6, $searchString);
        $query->bindValue(7, $searchString);
        $query->bindValue(8, $searchString);
        $query->bindValue(9, $searchString);
        $query->bindValue(10, $searchString);
        $query->bindValue(11, $searchString);
        $query->bindValue(12, $searchString);
        $query->bindValue(13, $searchString);
        $query->bindValue(14, $searchString);
        $query->bindValue(15, (int) $perPage, PDO::PARAM_INT);
        $query->bindValue(16, (int) $offset, PDO::PARAM_INT);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        // Count
        $sqlCount = 'SELECT * FROM recepcionistas WHERE ';
        $sqlCount = $sqlCount . $concat;

        $queryCount = $this->db->prepare($sqlCount);
        $queryCount->bindValue(1, $searchString);
        $queryCount->bindValue(2, $searchData);
        $queryCount->bindValue(3, $searchString);
        $queryCount->bindValue(4, $searchString);
        $queryCount->bindValue(5, $searchString);
        $queryCount->bindValue(6, $searchString);
        $queryCount->bindValue(7, $searchString);
        $queryCount->bindValue(8, $searchString);
        $queryCount->bindValue(9, $searchString);
        $queryCount->bindValue(10, $searchString);
        $queryCount->bindValue(11, $searchString);
        $queryCount->bindValue(12, $searchString);
        $queryCount->bindValue(13, $searchString);
        $queryCount->bindValue(14, $searchString);

        if (!$queryCount->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'draw' => intval($data['page']),
            'recordsTotal' => intval($queryCount->rowCount()),
            'recordsFiltered' => intval($queryCount->rowCount()),
            'receptionists' => $result,
        ];
    }

    public function register($receptionist, $path)
    {
        try {
            $this->db->beginTransaction();

            // Check for duplicate fields
            $tables = ['recepcionistas', 'recepcionistas', 'recepcionistas', 'recepcionistas', 'login'];
            $fields = ['cpf', 'rg', 'celular', 'telefone', 'email'];
            $words = ['cpf', 'rg', 'celular', 'telefone', 'e-mail'];

            $checkMessage = $this->utils->checkDuplicates($receptionist, $tables, $fields, $words);

            if ($checkMessage) {throw new Exception('Campo(s) duplicado(s): ' . substr_replace($checkMessage, '', -2) . '.', 409);}

            // Login
            $resultLogin = $this->accessMapper->register($receptionist);

            // Receptionist
            $sql = 'INSERT INTO recepcionistas(login_id, nome, imagem, data_nascimento, cpf, rg, cep, estado, cidade, bairro, logradouro,
                    complemento, numero, celular, telefone, email) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $query = $this->db->prepare($sql);
            $query->bindValue(1, $resultLogin['lastInsertId']);
            $query->bindValue(2, $receptionist['nome']);
            $query->bindValue(3, $path);
            $query->bindValue(4, $this->utils->formatDateBrToEn($receptionist['dataNascimento']));
            $query->bindValue(5, $receptionist['cpf']);
            $query->bindValue(6, $receptionist['rg']);
            $query->bindValue(7, $receptionist['cep']);
            $query->bindValue(8, $receptionist['estado']);
            $query->bindValue(9, $receptionist['cidade']);
            $query->bindValue(10, $receptionist['bairro']);
            $query->bindValue(11, $receptionist['logradouro']);
            $query->bindValue(12, isset($receptionist['complemento']) ? $receptionist['complemento'] : null);
            $query->bindValue(13, $receptionist['numero']);
            $query->bindValue(14, $receptionist['celular']);
            $query->bindValue(15, isset($receptionist['telefone']) ? $receptionist['telefone'] : null);
            $query->bindValue(16, $receptionist['email']);

            if (!$query->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

            $this->db->commit();
            return [
                'status' => 201,
                'message' => 'Recepcionista cadastrada com sucesso.',
            ];

        } catch (Exception $e) {
            $this->db->rollBack();
            return [
                'status' => $e->getCode(),
                'message' => $e->getMessage(),
            ];
        }
    }

    public function update($receptionist, $path)
    {
        // Check for duplicate fields
        $tables = 'recepcionistas';
        $fields = ['cpf', 'rg', 'celular', 'telefone'];
        $words = ['cpf', 'rg', 'celular', 'telefone'];

        $checkMessage = $this->utils->checkDuplicates($receptionist, $tables, $fields, $words, $receptionist['id']);

        if ($checkMessage) {
            return [
                'status' => 409,
                'message' => 'Campo(s) duplicado(s): ' . substr_replace($checkMessage, '', -2) . '.',
            ];
        }

        // Receptionist
        $sql = 'UPDATE recepcionistas SET nome = ?, imagem = ?, data_nascimento = ?, cpf = ?, rg = ?, cep = ?, estado = ?,
                cidade = ?, bairro = ?, logradouro = ?, complemento = ?, numero = ?, celular = ?, telefone = ? WHERE id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $receptionist['nome']);
        $query->bindValue(2, $path);
        $query->bindValue(3, $this->utils->formatDateBrToEn($receptionist['dataNascimento']));
        $query->bindValue(4, $receptionist['cpf']);
        $query->bindValue(5, $receptionist['rg']);
        $query->bindValue(6, $receptionist['cep']);
        $query->bindValue(7, $receptionist['estado']);
        $query->bindValue(8, $receptionist['cidade']);
        $query->bindValue(9, $receptionist['bairro']);
        $query->bindValue(10, $receptionist['logradouro']);
        $query->bindValue(11, isset($receptionist['complemento']) ? $receptionist['complemento'] : null);
        $query->bindValue(12, $receptionist['numero']);
        $query->bindValue(13, $receptionist['celular']);
        $query->bindValue(14, isset($receptionist['telefone']) ? $receptionist['telefone'] : null);
        $query->bindValue(15, $receptionist['id']);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Recepcionista atualizada com sucesso.',
        ];
    }

    public function findById($id)
    {
        $sql = 'SELECT * FROM recepcionistas WHERE id = ? LIMIT 1';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        if ($query->rowCount() === 0) {
            return [
                'status' => 404,
                'message' => 'Recepcionista não encontrada',
            ];
        }

        $result = $query->fetch(\PDO::FETCH_ASSOC);

        $obj = new \stdClass;
        $obj->id = $result['id'];
        $obj->nome = $result['nome'];
        $obj->imagemAntiga = $result['imagem'];
        $obj->dataNascimento = $result['data_nascimento'];
        $obj->cpf = $result['cpf'];
        $obj->rg = $result['rg'];
        $obj->cep = $result['cep'];
        $obj->estado = $result['estado'];
        $obj->cidade = $result['cidade'];
        $obj->bairro = $result['bairro'];
        $obj->logradouro = $result['logradouro'];
        $obj->complemento = $result['complemento'];
        $obj->numero = $result['numero'];
        $obj->celular = $result['celular'];
        $obj->telefone = $result['telefone'];

        return [
            'status' => 200,
            'message' => 'Recepcionista encontrada com sucesso.',
            'receptionist' => $obj,
        ];
    }

    public function updateStatus($id, $status)
    {
        $sql = 'UPDATE recepcionistas SET status = ? WHERE id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $status);
        $query->bindValue(2, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Recepcionista ' . ($status === '1' ? 'ativada' : 'desativada') . ' com sucesso.',
        ];
    }

}
