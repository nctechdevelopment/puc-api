<?php
namespace App\Models\Mappers;

use App\Core\Database;
use App\Helpers\Utils;
use App\Models\Mappers\AccessMapper;
use Exception;
use PDO as PDO;

class ScheduleMapper
{
    /**
     * PDO Object to interact with database
     *
     * @var PDO
     */
    private $db;

    /**
     * Other variables
     */
    private $utils;
    private $accessMapper;

    /**
     * Constructor of the class
     */
    public function __construct()
    {
        // Opening database connection
        $this->db = new Database();
        $this->db = $this->db->getInstance();
        // Other variables
        $this->utils = new Utils();
        $this->accessMapper = new AccessMapper();
    }

    public function register($data)
    {
        try {
            $this->db->beginTransaction();

            // Dates
            $start = $this->utils->formatDateTimeBrToEn($data['dataInicio']);
            $end = $this->utils->formatDateTimeBrToEn($data['dataFim']);

            // Register
            $sql = 'INSERT INTO agendamentos (titulo, pacientes_id, medicos_id, tipo,
                data_inicio, data_fim) VALUES (?, ?, ?, ?, ?, ?)';
            $query = $this->db->prepare($sql);
            $query->bindValue(1, $data['titulo']);
            $query->bindValue(2, $data['paciente']['id']);
            $query->bindValue(3, $data['medico']['id']);
            $query->bindValue(4, $data['tipo']);
            $query->bindValue(5, $start);
            $query->bindValue(6, $end);

            if (!$query->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

            $this->db->commit();
            return [
                'status' => 200,
                'message' => 'Agendamento cadastrado com sucesso.',
            ];
        } catch (Exception $e) {
            $this->db->rollBack();
            return [
                'status' => $e->getCode(),
                'message' => $e->getMessage(),
            ];
        }
    }

    public function findAllDoctors()
    {
        $sql = 'SELECT id, nome, imagem FROM medicos WHERE status = "1"';
        $query = $this->db->prepare($sql);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        return [
            'status' => 200,
            'message' => 'Médico encontrado com sucesso.',
            'doctors' => $result,
        ];
    }

    public function findAllByDoctor($id, $start, $end)
    {
        $sql = 'SELECT a.*
                FROM agendamentos AS `a`
                JOIN medicos AS `m` ON m.id = a.medicos_id
                WHERE a.medicos_id = ? AND a.data_inicio BETWEEN DATE(?) AND DATE(?)';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);
        $query->bindValue(2, $start);
        $query->bindValue(3, $end);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        $arrayObj = array();
        foreach ($result as $schedule) {
            $obj = new \stdClass;
            $obj->id = $schedule['id'];
            $obj->titulo = $schedule['titulo'];
            $obj->tipo = $schedule['tipo'];
            $obj->status = $schedule['status'];
            $obj->dataInicio = $schedule['data_inicio'];
            $obj->dataFim = $schedule['data_fim'];
            $obj->dataAgendamento = $schedule['data_agendamento'];
            $obj->paciente['id'] = $schedule['pacientes_id'];
            $obj->medico['id'] = $schedule['medicos_id'];
            $arrayObj[] = $obj;
        }

        return [
            'status' => 200,
            'message' => 'Agendamentos encontrados com sucesso.',
            'schedules' => $arrayObj,
        ];
    }

    public function findById($id)
    {
        $sql = 'SELECT * FROM agendamentos WHERE id  = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        if ($query->rowCount() === 0) {
            return [
                'status' => 404,
                'message' => 'Agendamento não encontrado.',
            ];
        }

        $result = $query->fetch(\PDO::FETCH_ASSOC);

        $obj = new \stdClass;
        $obj->id = $result['id'];
        $obj->titulo = $result['titulo'];
        $obj->tipo = $result['tipo'];
        $obj->status = $result['status'];
        $obj->dataInicio = $result['data_inicio'];
        $obj->dataFim = $result['data_fim'];
        $obj->dataAgendamento = $result['data_agendamento'];
        $obj->paciente['id'] = $result['pacientes_id'];
        $obj->medico['id'] = $result['medicos_id'];

        return [
            'status' => 200,
            'message' => 'Agendamento encontrado com sucesso.',
            'schedule' => $obj,
        ];
    }

    public function update($id, $data)
    {
        $sql = 'UPDATE agendamentos SET titulo = ?, pacientes_id = ?, medicos_id = ?, tipo = ? WHERE id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $data['titulo']);
        $query->bindValue(2, $data['paciente']['id']);
        $query->bindValue(3, $data['medico']['id']);
        $query->bindValue(4, $data['tipo']);
        $query->bindValue(5, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Agendamento atualizado com sucesso.',
        ];
    }

    public function updateEvent($id, $data)
    {
        try {
            $this->db->beginTransaction();

            // Dates
            $start = $this->utils->formatDateTimeBrToEn($data['dataInicio']);
            $end = $this->utils->formatDateTimeBrToEn($data['dataFim']);

            // Update
            $sql = 'UPDATE agendamentos SET data_inicio = ?, data_fim = ? WHERE id = ?';
            $query = $this->db->prepare($sql);
            $query->bindValue(1, $data['dataInicio']);
            $query->bindValue(2, $data['dataFim']);
            $query->bindValue(3, $id);

            if (!$query->execute()) {throw new Exception('Desculpe, ocorreu um erro interno.', 500);}

            $this->db->commit();
            return [
                'status' => 200,
                'message' => 'Agendamento atualizado com sucesso.',
            ];
        } catch (Exception $e) {
            $this->db->rollBack();
            return [
                'status' => $e->getCode(),
                'message' => $e->getMessage(),
            ];
        }
    }

    public function delete($id)
    {
        $sql = 'DELETE FROM agendamentos WHERE id = ?';
        $query = $this->db->prepare($sql);
        $query->bindValue(1, $id);

        if (!$query->execute()) {
            return [
                'status' => 500,
                'message' => 'Desculpe, ocorreu um erro interno.',
            ];
        }

        return [
            'status' => 200,
            'message' => 'Agendamento deletado com sucesso.',
        ];
    }

}
