# API

## Installation Instructions

1. Clone the repository.
2. Enter repository's folder and run `composer install`.
3. Open sample.config.php file and update with your environment data.
4. Rename sample.config.php to config.php.

---

## Change notes

v.1 - Initial version
v.2 - Installation Instructions and removing other infos.

---
