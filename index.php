<?php

require_once 'vendor/autoload.php';
require_once 'App/Helpers/Cors.php';

use App\Acl\Authorized;
use App\Controllers\AccessController;
use App\Controllers\DoctorController;
use App\Controllers\LaboratoryController;
use App\Controllers\MedicamentController;
use App\Controllers\MedicalController;
use App\Controllers\ReceptionistController;
use App\Controllers\ScheduleController;
use App\Controllers\PatientController;
use \Slim\App as App;

$config = include dirname(__FILE__) . '/config.php';
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

$container = new \Slim\Container($configuration);

$app = new App($container);

$app->add(new Tuupola\Middleware\JwtAuthentication([
    'secure' => false,
    'path' => '/api',
    'ignore' => [
        '/api/access/login',
    ],
    'header' => 'Authorization',
    'secret' => $config['secret_key'],
    'attribute' => 'decoded_token_data',
    'error' => function ($response, $arguments) {
        $data['status'] = 401;
        $data['token_error'] = true;
        $data['message'] = $arguments['message'];
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->getBody()->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    },
]));

// Access endpoints
$app->group('/access', function () use ($app) {
    $app->post('/login', AccessController::class . ':login');
    $app->post('/login/exam', AccessController::class . ':loginExam');
});

// API group
$app->group('/api', function () use ($app) {

    // Access endpoints
    $app->group('/access', function () use ($app) {
        $app->get('/refresh-token', AccessController::class . ':refreshToken');
        $app->put('/change-password/{id}', AccessController::class . ':changePassword');
        $app->put('/change-password/{id}/secure', AccessController::class . ':changePasswordSafely');
    });

    // Doctor endpoints
    $app->group('/doctor', function () use ($app) {
        $app->post('/search', DoctorController::class . ':searchAll');
        $app->post('/register', DoctorController::class . ':register');
        $app->post('/update', DoctorController::class . ':update');
        $app->get('/{id}', DoctorController::class . ':findById');
        $app->put('/{id}/status/{status}', DoctorController::class . ':updateStatus');
    });

    // Laboratory endpoints
    $app->group('/laboratory', function () use ($app) {
        $app->post('/search', LaboratoryController::class . ':searchAll');
        $app->post('/register', LaboratoryController::class . ':register');
        $app->post('/update', LaboratoryController::class . ':update');
        $app->get('/{id}', LaboratoryController::class . ':findById');
        $app->put('/{id}/status/{status}', LaboratoryController::class . ':updateStatus');
    });

    // Medicament endpoints
    $app->group('/medicament', function () use ($app) {
        $app->post('/search', MedicamentController::class . ':searchAll');
        $app->post('/register', MedicamentController::class . ':register');
        $app->put('/update/{id}', MedicamentController::class . ':update');
        $app->get('/{id}', MedicamentController::class . ':findById');
        $app->get('/filter/{search}', MedicamentController::class . ':search');
        $app->put('/{id}/status/{status}', MedicamentController::class . ':updateStatus');
    });

    // Receptionist endpoints
    $app->group('/receptionist', function () use ($app) {
        $app->post('/search', ReceptionistController::class . ':searchAll');
        $app->post('/register', ReceptionistController::class . ':register');
        $app->post('/update', ReceptionistController::class . ':update');
        $app->get('/{id}', ReceptionistController::class . ':findById');
        $app->put('/{id}/status/{status}', ReceptionistController::class . ':updateStatus');
    });

    // Schedule endpoints
    $app->group('/schedule', function () use ($app) {
        $app->post('', ScheduleController::class . ':register');
        $app->get('/doctors', ScheduleController::class . ':findAllDoctors');
        $app->get('/{id}/{start}/{end}', ScheduleController::class . ':findAllByDoctor');
        $app->get('/{id}', ScheduleController::class . ':findById');
        $app->put('/{id}', ScheduleController::class . ':update');
        $app->put('/event/{id}', ScheduleController::class . ':updateEvent');
        $app->delete('/{id}', ScheduleController::class . ':delete');
    });

    // Medical record endpoints
    $app->group('/medical', function () use ($app) {
        $app->post('', MedicalController::class . ':registerMedical');
        $app->post('/upload/exam', MedicalController::class . ':uploadExam');
        $app->get('/download/exam/{id}', MedicalController::class . ':downloadExam');
        $app->get('/record/schedule/{id}', MedicalController::class . ':findRecordByScheduleId');
        $app->get('/record/historic/patient/{id}/page/{page}', MedicalController::class . ':findRecordHistoricByPatientId');
        $app->get('/exam/record/{id}', MedicalController::class . ':findExamByRecordId');
        $app->get('/exam/historic/patient/{id}/page/{page}', MedicalController::class . ':findExamHistoricByPatientId');
        $app->get('/prescription/record/{id}', MedicalController::class . ':findPrescriptionByRecordId');
        $app->get('/prescription/historic/patient/{id}/page/{page}', MedicalController::class . ':findPrescriptionHistoricByPatientId');
        $app->get('/certificate/record/{id}', MedicalController::class . ':findCertificateByRecordId');
        $app->get('/certificate/historic/patient/{id}/page/{page}', MedicalController::class . ':findCertificateHistoricByPatientId');
        $app->get('/filter/exam-types/{search}', MedicalController::class . ':searchExamTypes');
    });

    // Patient endpoints
    $app->group('/patient', function () use ($app) {
        $app->post('/search', PatientController::class . ':searchAll');
        $app->post('/register', PatientController::class . ':register');
        $app->post('/update', PatientController::class . ':update');
        $app->get('/{id}', PatientController::class . ':findById');
        $app->get('/filter/{search}', PatientController::class . ':search');
        $app->put('/{id}/status/{status}', PatientController::class . ':updateStatus');
    });

})->add(new Authorized());

$app->run();
