<?php
/**
 * This file has information used to setting up the application
 *
 * @category Config
 * @package ConfigDevelopment
 * @license All rights reserverd to WeCode.
 */

return array(
    "db" => array(
        "host" => "host",
        "user" => "user",
        "password" => "password",
        "database" => "dbname",
    ),
    "server_name" => "server_name",
    "secret_key" => "secret_key",
);
